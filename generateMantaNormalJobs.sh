####
SCHEDULER="SLURM"


#### Parameters to set
BATCHDIR="batch5"
PROJECTDIR="/hpc/pmc_kuiper/DNARepairAndCongenitalImmunodeficiencySyndromes/"
JOBDIR="$PROJECTDIR/CODE/jobs/manta/$BATCHDIR/"
OUTPUTDIR="$PROJECTDIR/ANALYSIS/manta/$BATCHDIR/"
REFGENOME="/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta"
SAMPLEFILE="$PROJECTDIR/samplelists/sampleListMutect2.$BATCHDIR.Converted.txt"

####

DATE=$(date --iso-8601=seconds)

mkdir -p $JOBDIR
mkdir -p $OUTPUTDIR

#PMRBM000AFA_BHHL7CDSXX_S4_L001_R1_001.fastq.gz

#/hpc/pmc_kuiper/MRD4ALL/DATA/WGS/191031_HMFreg0588_FR19448641_8478CR1/8478CR1_HVMKMDSXX_S29_L001_R1_001.fastq.gz

while read line
do

	NORMAL=$( echo $line | awk '{print $1}')
	TUMOR=$( echo $line | awk '{print $2}')

	SAMPLE=$NORMAL
	BAM="$PROJECTDIR/RESULTS/GermlineCalling/$SAMPLE/call-GatherBamFiles/$SAMPLE\_WGS.bam"

echo "Generating job for sample $SAMPLE ..";

##########
####CREATE JOB HEADER

JOBNAME="sample.$SAMPLE.manta.sh"
OUTPUTLOG="$JOBDIR/sample.$SAMPLE.manta.sh.out"
ERRORLOG="$JOBDIR/sample.$SAMPLE.manta.sh.err"
WALLTIME="05:59:00"
NUMTASKS="1"
NUMCPUS="26"
MEM="60G"
TMPSPACE="300G"
JOBFILE="$JOBDIR/sample.$SAMPLE.manta.sh"


if [[ $SCHEDULER == "SLURM" ]]
then
    cat <<- EOF > $JOBFILE
#!/bin/bash
#SBATCH --job-name=$JOBNAME
#SBATCH --output=$OUTPUTLOG
#SBATCH --error=$ERRORLOG
#SBATCH --partition=cpu
#SBATCH --time=$WALLTIME
#SBATCH --ntasks=$NUMTASKS
#SBATCH --cpus-per-task $NUMCPUS
#SBATCH --mem=$MEM
#SBATCH --gres=tmpspace:$TMPSPACE
#SBATCH --nodes=1
#SBATCH --open-mode=append

EOF

elif [[ $SCHEDULER == "SGE" ]]
then

    cat <<- EOF > $JOBFILE
#$ -S /bin/bash
#$ -N $JOBNAME
#$ -o $OUTPUTLOG
#$ -e $ERRORLOG
#$ -l h_rt=$WALLTIME
#$ -l h_vmem=$MEM
#$ -pe threaded $NUMCPUS
#$ -l tmpspace=$TMPSPACE
#$ -cwd


EOF

else
    echo "Type of scheduler not known: $SCHEDULER"
    exit
fi


    
echo "


set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"

#Load module
module load manta/1.6.0
module load python/2.7.10
module load samtools/1.3
module load R/3.6.1

#Make output dir
mkdir -p $OUTPUTDIR/$SAMPLE/

cd $OUTPUTDIR/$SAMPLE/

#..manta configuration..
python /hpc/local/CentOS7/gen/software/manta/1.6.0/bin/configManta.py \\
--bam=$BAM \\
--referenceFasta=$REFGENOME \\
--callRegions=$PROJECTDIR/DATA/SV_analysis/GRCh38.bed.gz \\
--runDir=$OUTPUTDIR/$SAMPLE/

#..run manta..
python $OUTPUTDIR/$SAMPLE/runWorkflow.py -j 25

#..reformat inversions into single inverted sequence junctions..
python /hpc/local/CentOS7/gen/software/manta/1.6.0/libexec/convertInversion.py \\
/hpc/local/CentOS7/gen/software/samtools-1.3/bin/samtools \\
$REFGENOME \\
$OUTPUTDIR/$SAMPLE/results/variants/diploidSV.vcf.gz \\
> $OUTPUTDIR/$SAMPLE/results/variants/tumorSV_v2.vcf

# Run rscript to annotate results
Rscript /hpc/pmc_kuiper/References/scripts/Manta_parser_cmdline.Rscript \\
$OUTPUTDIR/$SAMPLE/results/variants/tumorSV_v2.vcf \\
$OUTPUTDIR/$SAMPLE/results/variants/$SAMPLE.manta.output.annotated.txt

cd -

#Generate md5sums
cd $OUTPUTDIR/$SAMPLE/results/variants/

md5sum diploidSV.vcf.gz > diploidSV.vcf.gz.md5
md5sum candidateSV.vcf.gz > candidateSV.vcf.gz.md5
md5sum candidateSmallIndels.vcf.gz > candidateSmallIndels.vcf.gz.md5
gzip tumorSV_v2.vcf
md5sum tumorSV_v2.vcf.gz > tumorSV_v2.vcf.gz.md5
md5sum $SAMPLE.manta.output.annotated.txt > $SAMPLE.manta.output.annotated.txt.md5

cd -

#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

" >> $JOBFILE

done<$SAMPLEFILE

