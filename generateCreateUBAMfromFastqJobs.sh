
BATCHDIR="batch1/"
PROJECTDIR="/hpc/pmc_kuiper/fvandijk2/projects/<PROJECTNAME>/"
DATADIR="$PROJECTDIR/DATA/fastq/"
JOBDIR="$PROJECTDIR/CODE/jobs/createUBAMfromFastq/$BATCHDIR/"
OUTPUTDIR="$PROJECTDIR/ANALYSIS/createUBAMfromFastq/$BATCHDIR/"

DATE=$(date --iso-8601=seconds)

mkdir -p $JOBDIR
mkdir -p $OUTPUTDIR

#SAMPLEID_FLOWCELLID_SNUM_LANENUMBER_READ_001.fastq.gz

#Change file extension if needed!!
for FASTQ in $( ls $DATADIR/*R1_001.fastq.gz)
do
    #Extract run information from filename and build readgroup tag, etc.
    PREFIX=$( basename $FASTQ _R1_001.fastq.gz )
    BASENAME=$( basename $FASTQ .fastq.gz )
    SAMPLE=$( echo $BASENAME | awk '{print $1}' FS="_" )
    FLOWCELL=$( echo $BASENAME | awk '{print $2}' FS="_" )
    SNUM=$( echo $BASENAME | awk '{print $3}' FS="_" )
    LANE=$( echo $BASENAME | awk '{print $4}' FS="_" )
    READ=$( echo $BASENAME | awk '{print $5}' FS="_" )
    NUM=$( echo $BASENAME | awk '{print $6}' FS="_" )
    READGROUP=$FLOWCELL\_$LANE
    OUTPUTPREFIX=$SAMPLE\_$FLOWCELL\_$SNUM\_$LANE\_$NUM

echo "
#$ -S /bin/bash
#$ -N sample.$OUTPUTPREFIX.createUBAMfromFastq.sh
#$ -l h_vmem=12G
#$ -l tmpspace=260G
#$ -l h_rt=07:59:00
#$ -o $JOBDIR/$OUTPUTPREFIX.createUBAMfromFastq.sh.out
#$ -e $JOBDIR/$OUTPUTPREFIX.createUBAMfromFastq.sh.err
#$ -cwd

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"

#Load module
module load picardtools/2.10.10

#Run command
java -Xmx8g -Djava.io.tmpdir=\$TMPDIR -jar \$PICARD FastqToSam \\
FASTQ="$DATADIR/$PREFIX\_R1_001.fastq.gz" \\
FASTQ2="$DATADIR/$PREFIX\_R2_001.fastq.gz" \\
OUTPUT=$OUTPUTDIR/$OUTPUTPREFIX.ubam \\
READ_GROUP_NAME=$READGROUP \\
SAMPLE_NAME=$SAMPLE \\
LIBRARY_NAME=$SAMPLE \\
PLATFORM=ILLUMINA \\
SEQUENCING_CENTER=UMCU \\
RUN_DATE=$DATE

#Create md5 sum
cd $OUTPUTDIR/
md5sum $OUTPUTPREFIX.ubam > $OUTPUTPREFIX.ubam.md5
cd -


#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

" > $JOBDIR/$OUTPUTPREFIX.createUBAMfromFastq.sh

done
