

BATCHDIR="batch4/"
PROJECTDIR="/<projectHomeDir>/"
PIPELINEDIR="$PROJECTDIR/CODE/pmc_kuiper_pipelines/wdl_pipeline_v2.1.0/wdl/"
INPUTDIR="$PROJECTDIR/CODE/runConfigs/GermlineSingleSample/$BATCHDIR/"


#Zip pipeline and dependencies
cd $PIPELINEDIR
zip workflowDependencies.zip \
cnv_common_tasks.wdl \
dataprep.wdl \
getVersion.wdl \
simpleChecks.wdl \
picard.wdl \
structureConversion.wdl \
align.wdl \
qc.wdl \
genomicIntervals.wdl \
gatk.wdl \
samtools.wdl \
bedTools.wdl \
ensemblVep.wdl \
map_keys.wdl \
dataconnections.wdl \
annotate.wdl \
molgenis.wdl
cd -



#Move dependencies zip to json directory
mv $PIPELINEDIR/workflowDependencies.zip $INPUTDIR

#Execute pipeline using cromwell
for JSON in $( ls $INPUTDIR/sample.*.json )
do

echo "$JSON"
    
curl -X POST "https://<username>.<hostname>/api/workflows/v1" \
  --user <username>:<password> \
    --header "accept: application/json" \
    --header "Content-Type: multipart/form-data" \
    --form "workflowSource=@$PIPELINEDIR/germline_single_sample_workflow.wdl" \
    --form "workflowInputs=@$JSON" \
    --form "workflowDependencies=@$INPUTDIR/workflowDependencies.zip"

echo ""

done




