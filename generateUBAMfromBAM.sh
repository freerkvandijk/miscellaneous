

PROJECTDIR="/hpc/pmc_kuiper/fvandijk2/projects/<PROJECTNAME>/"
DATADIR="$PROJECTDIR/DATA/BAM/"
JOBDIR="$PROJECTDIR/CODE/jobs/createUBAMfromBAM/"
OUTPUTDIR="$PROJECTDIR/ANALYSIS/createUBAMfromBAM/"

DATE=$(date --iso-8601=seconds)

mkdir -p $JOBDIR
mkdir -p $OUTPUTDIR



for BAM in $( ls $DATADIR/*dedup.reformatted.bam)
do

    SAMPLE=$( basename $BAM _dedup.reformatted.bam )


echo "
#$ -S /bin/bash
#$ -N sample.$SAMPLE.createUBAMfromBAM.sh
#$ -l h_vmem=12G
#$ -l tmpspace=200G
#$ -l h_rt=15:59:00
#$ -l h=!n0088.compute.hpc
#$ -o $JOBDIR/$SAMPLE.createUBAMfromBAM.sh.out
#$ -e $JOBDIR/$SAMPLE.createUBAMfromBAM.sh.err
#$ -cwd

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"

#Load module
module load picardtools/2.10.10

#Run command
java -Xmx8g -Djava.io.tmpdir=\$TMPDIR -jar \$PICARD RevertSam \\
    I=$BAM \\
    O=$OUTPUTDIR/$SAMPLE.revertSam.ubam \\
    SANITIZE=true \\
    MAX_DISCARD_FRACTION=0.005 \\
    ATTRIBUTE_TO_CLEAR=XT \\
    ATTRIBUTE_TO_CLEAR=XN \\
    ATTRIBUTE_TO_CLEAR=AS \\
    ATTRIBUTE_TO_CLEAR=OC \\
    ATTRIBUTE_TO_CLEAR=OP \\
    SORT_ORDER=queryname \\
    RESTORE_ORIGINAL_QUALITIES=true \\
    REMOVE_DUPLICATE_INFORMATION=true \\
    REMOVE_ALIGNMENT_INFORMATION=true



#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

" > $JOBDIR/$SAMPLE.createUBAMfromBAM.sh

done
