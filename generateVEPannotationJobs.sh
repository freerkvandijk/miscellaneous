
### General vars
SCHEDULER="SLURM"
PROJECTDIR="/hpc/pmc_kuiper/GeneticPredisposition_Single_Cases/"
OUTPUT_DIR="$PROJECTDIR/ANALYSIS/VEPannotation/"
JOB_DIR="$PROJECTDIR/CODE/jobs/VEPannotation/"
VCF_EXT="_WXS.vcf.gz"

### Annotation specific vars
#define global variables
REFERENCE_DIR="/hpc/pmc_kuiper/References/"
DIR_CACHE="$REFERENCE_DIR/vep92/cache/"
FASTA_FILE="/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta"
#FASTA_FILE="$REFERENCE_DIR/hg38/decode/genome.fa"
PLUGIN_PATH="$REFERENCE_DIR/vep92/plugin/"
NUM_FORKS=4 # recommended by the VEP team to use 4 forks
#NUM_CORES=4

mkdir -p $JOB_DIR
mkdir -p $OUTPUT_DIR


for VCF in $(ls $PROJECTDIR/RESULTS/*/call-CombineVCF_WXS/*${VCF_EXT})
do

INPUT_FILE="$VCF"


SAMPLE_ID=$( basename $VCF $VCF_EXT )


# we expect a vcf format. TODO buildin checks!
file_name=$(basename $VCF $VCF_EXT)
output_file=$OUTPUT_DIR/$file_name.vepAnnotated.vcf
output_file_tab=$OUTPUT_DIR/$file_name.vepAnnotated.tab.txt
output_file_tab_sorted=$OUTPUT_DIR/$file_name.vepAnnotated.sortedColumns.tab.txt

#For testing purpose only!!
rm -r $OUTPUT_DIR/$file_name*

echo -e "Creating vep annotation job for sample $SAMPLE_ID"


##########
####CREATE JOB HEADER

JOBNAME="runVepAnnotion.$SAMPLE_ID"
OUTPUTLOG="$JOB_DIR/$SAMPLE_ID-vepAnnotation.sh.out"
ERRORLOG="$JOB_DIR/$SAMPLE_ID-vepAnnotation.sh.err"
WALLTIME="23:59:00"
NUMTASKS=1
NUMCPUS=$NUM_FORKS
MEM="16G"
TMPSPACE="39G"
JOBFILE="$JOB_DIR/$SAMPLE_ID-vepAnnotation.sh"

if [[ $SCHEDULER == "SLURM" ]]
then
    cat <<- EOF > $JOBFILE
#!/bin/bash
#SBATCH --job-name=$JOBNAME
#SBATCH --output=$OUTPUTLOG
#SBATCH --error=$ERRORLOG
#SBATCH --partition=cpu
#SBATCH --time=$WALLTIME
#SBATCH --ntasks=$NUMTASKS
#SBATCH --cpus-per-task $NUMCPUS
#SBATCH --mem=$MEM
#SBATCH --gres=tmpspace:$TMPSPACE
#SBATCH --nodes=1
#SBATCH --open-mode=append

EOF

elif [[ $SCHEDULER == "SGE" ]]
then

    cat <<- EOF > $JOBFILE
#$ -S /bin/bash
#$ -cwd
#$ -o $OUTPUTLOG
#$ -e $ERRORLOG
#$ -N $JOBNAME
#$ -l h_rt=$WALLTIME
#$ -l h_vmem=$MEM
#$ -l tmpspace=$TMPSPACE
#$ -pe threaded $NUMCPUS

EOF

else
    echo "Type of scheduler not known: $SCHEDULER"
    exit
fi





echo -e """

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"

echo \"Starting annotation..\"

# load the required modules
module load ensemblvep/92
module load samtools/1.9

# vep command line
vep \\
--offline \\
--cache \\
--fork $NUM_FORKS \\
--dir_cache $DIR_CACHE \\
--dir_plugins $PLUGIN_PATH \\
--assembly GRCh38 \\
--input_file $INPUT_FILE \\
--output_file $output_file \\
--force_overwrite \\
--fasta $FASTA_FILE \\
--vcf \\
--pick \\
--pick_order tsl,appris,rank \\
--custom $REFERENCE_DIR/goNL/hg38/multisample.parents_only.info_only.vcf.gz,goNL,vcf,exact,0,AC,AN,AF \\
--custom $REFERENCE_DIR/gnomAD/gnomadGenomesHg38/genomewide/gnomad.genomes.r3.0.sites.vcf.gz,gnomADg,vcf,exact,0,AC,AN,AF,AF_male,AF_female,AF_afr,AF_amr,AF_asj,AF_eas,AF_fin,AF_nfe,AF_ami,AF_sas,AF_oth \\
--custom $REFERENCE_DIR/Cosmic/v91/CosmicVariants.normal.merged.vcf.gz,cosmic_v91,vcf,exact,GENOMIC_ID,COSMIC_ID,LEGACY_ID,CNT,SNP,HGVSC,HGVSG,HGVSP \\
--custom $REFERENCE_DIR/clinvar/clinvar_20200210.vcf.gz,ClinVar,vcf,exact,0,CLNALLELEID,CLNDN,CLNDISDB,CLNREVSTAT,CLNSIG,AF_TGP \\
--custom $REFERENCE_DIR/phyloP/hg38/hg38.phyloP100way.bed.gz,phyloP100way,bed \\
--custom $REFERENCE_DIR/phyloP/hg38/hg38.phyloP20way.bed.gz,phyloP20way,bed \\
--custom $REFERENCE_DIR/dbSNP/All_20180418.vcf.gz,dbSNP151,vcf,exact,0,dbSNPBuildID \\
--custom $REFERENCE_DIR/annotation/cytoband/hg38.cytoband.bed.gz,cytoband,bed \\
--custom $REFERENCE_DIR/vep92/pluginReferences/gnomad.v2.1.1.hg38coordinates.pLIscores.bed.gz,gnomAD_pLI,bed \\
--custom $REFERENCE_DIR/GWAScatalog/gwas_catalog_v1.0.2-associations_e98_r2020-02-08.vcf.gz,GWAScatalogue,vcf,overlap,0,PUBMEDID,LINK,DISEASE/TRAIT,STRONGEST_SNP-RISK_ALLELE \\
--custom $REFERENCE_DIR/OMIM/generated_on_2020-03-02/genemap2.vcf.gz,OMIM,vcf,overlap,0,Gene_Name,Gene_Symbols,MIM_Number,Comments,Phenotypes \\
--plugin CADD,$REFERENCE_DIR/cadd/hg38/whole_genome_SNVs.tsv.gz,/hpc/pmc_kuiper/References/cadd/hg38/InDels.tsv.gz \\
--plugin MaxEntScan,/hpc/pmc_kuiper/References/maxEntScan/maxEntScan/,SWA,NCSS \\
--plugin SpliceAI,snv=/hpc/pmc_kuiper/References/spliceAI/spliceai_scores.masked.snv.hg38.vcf.gz,indel=/hpc/pmc_kuiper/References/spliceAI/spliceai_scores.masked.indel.hg38.vcf.gz \\
--plugin GeneSplicer,/hpc/pmc_kuiper/References/geneSplicer/geneSplicer/bin/linux/genesplicer,/hpc/pmc_kuiper/References/geneSplicer/geneSplicer/human/,context=200,cache_size=50 \\
--plugin LoF,loftee_path:$REFERENCE_DIR/vep92/plugin/,human_ancestor_fa:$REFERENCE_DIR/vep92/pluginReferences/loftee/hg38/human_ancestor.fa.gz,exonic_denovo_only:0 \\
--plugin ExACpLI,$REFERENCE_DIR/vep92/pluginReferences/ExACpLI_values.txt \\
--plugin dbscSNV,$REFERENCE_DIR/dbscSNV/hg38/dbscSNV1.1_GRCh38.txt.gz \\
--plugin dbNSFP,$REFERENCE_DIR/dbNSFP/dbNSFP4.0a.gz,SIFT_score,SIFT_converted_rankscore,SIFT_pred,Polyphen2_HDIV_score,Polyphen2_HDIV_rankscore,Polyphen2_HDIV_pred,Polyphen2_HVAR_score,Polyphen2_HVAR_rankscore,Polyphen2_HVAR_pred,MetaSVM_score,MetaSVM_rankscore,MetaSVM_pred,Interpro_domain


echo \"Starting conversion to tabular\"

#Do we need to remove gnomADg_AF_asj (ashkenazi jews) from the popmax?
#Convert to tab
perl /hpc/pmc_kuiper/AnnotationPipeline/CODE/convertVEPannotatedVCFtoTable.pl \\
-vcf $output_file \\
-tab $output_file_tab \\
-type singlesample \\
-sample $SAMPLE_ID \\
-popmax=gnomADg_AF_afr,gnomADg_AF_amr,gnomADg_AF_eas,gnomADg_AF_nfe,gnomADg_AF_sas,gnomADg_AF_oth



echo \"Starting bgzip and tabix\"

#Create md5sums and do gzipping
cd $OUTPUT_DIR

bgzip $file_name.vepAnnotated.vcf
tabix $file_name.vepAnnotated.vcf.gz

gzip $file_name.vepAnnotated.tab.txt


echo \"Starting md5summing..\"

md5sum $file_name.vepAnnotated.vcf.gz > $file_name.vepAnnotated.vcf.gz.md5
md5sum $file_name.vepAnnotated.tab.txt.gz > $file_name.vepAnnotated.tab.txt.gz.md5

cd -


perl /hpc/pmc_kuiper/AnnotationPipeline/CODE/createCustomVEPannotatedTable.pl \\
-input $OUTPUT_DIR/$file_name.vepAnnotated.tab.txt.gz \\
-output $output_file_tab_sorted \\
-columns CHROM,POS,REF,ALT,QUAL,FILTER,${SAMPLE_ID}.GT,${SAMPLE_ID}.DP,\\
${SAMPLE_ID}.REFCOUNT,${SAMPLE_ID}.ALTCOUNT,${SAMPLE_ID}.VAF,\\
SYMBOL,Gene,dbSNP151,cytoband,\\
Consequence,IMPACT,\\
popMax,gnomADg,gnomADg_AC,gnomADg_AN,gnomADg_AF,gnomADg_AF_male,gnomADg_AF_female,\\
gnomADg_AF_afr,gnomADg_AF_amr,gnomADg_AF_asj,gnomADg_AF_eas,gnomADg_AF_fin,gnomADg_AF_nfe,\\
gnomADg_AF_ami,gnomADg_AF_sas,gnomADg_AF_oth,goNL,goNL_AC,goNL_AN,goNL_AF,\\
CADD_PHRED,CADD_RAW,gnomAD_pLI,ada_score,rf_score,Interpro_domain,MetaSVM_pred,MetaSVM_rankscore,\\
MetaSVM_score,Polyphen2_HDIV_pred,Polyphen2_HDIV_rankscore,Polyphen2_HDIV_score,Polyphen2_HVAR_pred,\\
Polyphen2_HVAR_rankscore,Polyphen2_HVAR_score,SIFT_converted_rankscore,SIFT_pred,SIFT_score,\\
MaxEntScan_alt,MaxEntScan_diff,MaxEntScan_ref,SpliceAI_pred_DP_AG,SpliceAI_pred_DP_AL,SpliceAI_pred_DP_DG,SpliceAI_pred_DP_DL,SpliceAI_pred_DS_AG,SpliceAI_pred_DS_AL,SpliceAI_pred_DS_DG,SpliceAI_pred_DS_DL,SpliceAI_pred_SYMBOL,GeneSplicer,\\
LoF,LoF_filter,LoF_flags,LoF_info,\\
cosmic_v91,cosmic_v91_COSMIC_ID,cosmic_v91_LEGACY_ID,cosmic_v91_CNT,cosmic_v91_SNP,\\
cosmic_v91_HGVSC,cosmic_v91_HGVSP,cosmic_v91_HGVSG,\\
ClinVar,ClinVar_CLNALLELEID,ClinVar_CLNDN,ClinVar_CLNDISDB,ClinVar_CLNREVSTAT,ClinVar_CLNSIG,ClinVar_AF_TGP,\\
phyloP100way,phyloP20way,\\
GWAScatalogue,GWAScatalogue_PUBMEDID,GWAScatalogue_LINK,GWAScatalogue_DISEASE/TRAIT,GWAScatalogue_STRONGEST_SNP-RISK_ALLELE,\\
OMIM_Gene_Name,OMIM_Gene_Symbols,OMIM_MIM_Number,OMIM_Comments,OMIM_Phenotypes,\\
Allele,Feature_type,Feature,BIOTYPE,EXON,INTRON,HGVSc,HGVSp,cDNA_position,CDS_position,\\
Protein_position,Amino_acids,Codons,Existing_variation,DISTANCE,STRAND,FLAGS,SYMBOL_SOURCE,\\
HGNC_ID,SOURCE,ExACpLI,dbSNP151_dbSNPBuildID,\\
AC,AF,AN,BaseQRankSum,DP,DS,END,ExcessHet,FS,InbreedingCoeff,MQ,MQRankSum,QD,ReadPosRankSum,SOR,VQSLOD,\\
${SAMPLE_ID}.AD,${SAMPLE_ID}.PGT,${SAMPLE_ID}.PL,${SAMPLE_ID}.SB


echo \"Starting md5summing..\"

md5sum $output_file_tab_sorted > $output_file_tab_sorted.md5

cd -


echo \"Finished all steps\"


#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
        
        echo -e \"Return code is zero, process was succesfull\n\n\"
        
else
  
        echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
        #Return non zero return code
        exit 1
        
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"







""" >> $JOBFILE

done


