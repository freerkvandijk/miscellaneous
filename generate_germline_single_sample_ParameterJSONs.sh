

####
BATCHDIR="batch15/"
PROJECTDIR="/hpc/pmc_kuiper/WESKidTs/"
PARAMDIR="$PROJECTDIR/CODE/runConfigs/GermlineSingleSample/$BATCHDIR/"
#INPUTDIR="$PROJECTDIR/ANALYSIS/createUBAMfromFastq/batch13/"
INPUTDIR="$PROJECTDIR/ANALYSIS/mergeUBAMsPerSample/$BATCHDIR/"

####
#EXPERIMENT_TYPE="Whole genome sequencing"
EXPERIMENT_TYPE="Exome sequencing"
#LIBRARY_STRATEGY="WGS"
LIBRARY_STRATEGY="WXS"

if [[ $LIBRARY_STRATEGY == "WXS" ]]
then
    TARGET_INTERVALS_BED_FILE="/hpc/pmc_kuiper/References/hg38bundle/v0/MedExome_hg38_capture_targets.bed"
    GAUSSIANSINDELS="2"
    GAUSSIANSSNPS="2"
elif [[ $LIBRARY_STRATEGY == "WGS" ]]
then
    TARGET_INTERVALS_BED_FILE=""
    GAUSSIANSINDELS="4"
    GAUSSIANSSNPS="4"
else
    echo "Type of library strategy not known: $LIBRARY_STRATEGY"
    exit
fi 
    
#
DATE=$(date --iso-8601=seconds)

mkdir -p "$PARAMDIR/"


for UBAM in $( ls $INPUTDIR/*.ubam )
do
    
    SAMPLENAME=$( basename $UBAM .ubam )
    SAMPLE=$( echo $SAMPLENAME | awk '{print $1}' FS="_" )
    MD5SUM=$( head -n1 $UBAM.md5 | awk '{print $1}' )
    

echo "
{
  \"GermlineSingleSample.biomaterial_id\": \"$SAMPLE\",
  \"GermlineSingleSample.experiment_ids\": [
    \"$SAMPLE\"
  ],
  \"GermlineSingleSample.lib_barcode_run_url_md5s\": [
    {
      \"url\": \"$UBAM\",
      \"md5\": \"$MD5SUM\",
      \"library\": \"None\",
      \"run\": \"$SAMPLE\"
    }
  ],
  \"GermlineSingleSample.submission_date\": \"$DATE\",
  \"GermlineSingleSample.library_strategy\": \"$LIBRARY_STRATEGY\",
  \"GermlineSingleSample.study\": \"$SAMPLE\",
  \"GermlineSingleSample.derived_data_path\": \"/data/isi/p/pmc_research/omics\",
  \"GermlineSingleSample.workflow_id\": \"germline_snv\",
  \"GermlineSingleSample.experiment_type\": \"$EXPERIMENT_TYPE\",
  \"GermlineSingleSample.data_host\": \"\",



  \"GermlineSingleSample.cache_path\": \"\",
  \"GermlineSingleSample.max_gaussians_indels\": \"${GAUSSIANSINDELS}\",
  \"GermlineSingleSample.max_gaussians_snps\": \"${GAUSSIANSSNPS}\",
  \"GermlineSingleSample.wallclock_ages\": \"36:00:00\",
  \"GermlineSingleSample.wallclock_long\": \"18:00:00\",
  \"GermlineSingleSample.wallclock_moderate\": \"12:00:00\",
  \"GermlineSingleSample.wallclock_short\": \"04:00:00\",
  \"GermlineSingleSample.tmpspace_gb_huge\": \"800\",
  \"GermlineSingleSample.tmpspace_gb_moderate\": \"80\",
  \"GermlineSingleSample.tmpspace_gb_small\": \"4\",

  \"GermlineSingleSample.vep_assembly\": \"GRCh38\",
  \"GermlineSingleSample.vep_species\": \"Homo sapiens\",
  \"GermlineSingleSample.vep_cpu\": \"4\",
  \"GermlineSingleSample.vep_annotations\": [
    {
      \"href\": \"/api/v1/pipeline_ensemblvep_annotation/aaaac4cshvhzuascvqjaadyaae\",
      \"id\": \"aaaac4cshvhzuascvqjaadyaae\",
      \"workflow_instance_id\": \"PMCWI000AAI\",
      \"label\": \"Diag_Germline_Gene\",
      \"file_id\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/genome_targets/hg38_diagnostic_germline_2.1.bed.gz\",
      \"index\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/genome_targets/hg38_diagnostic_germline_2.1.bed.gz.tbi\",
      \"filetype_id\": \"bed\",
      \"annotationtype\": \"overlap\",
      \"reportcoordinates\": 0,
      \"fields\": \"\"
    }
  ],
  \"GermlineSingleSample.vep_cache\": \"/hpc/pmc_kuiper/References/vep92/cache\",
  \"GermlineSingleSample.vep_fields\": [
    \"Location\",
    \"Allele\",
    \"VARIANT_CLASS\",
    \"Gene\",
    \"Feature\",
    \"SYMBOL\",
    \"CCDS\",
    \"STRAND\",
    \"IMPACT\",
    \"Consequence\",
    \"Amino_acids\",
    \"cDNA_position\",
    \"SIFT\",
    \"PolyPhen\",
    \"CADD_phred\",
    \"EXON\",
    \"DISTANCE\",
    \"Existing_variation\"
  ],
  \"GermlineSingleSample.vep_output_format\": \"vcf\",
  \"GermlineSingleSample.vep_plugin_dir\": \"/hpc/pmc_kuiper/References/vep92/plugin/\",
  \"GermlineSingleSample.vep_plugins\": [
    {
      \"href\": \"/api/v1/pipeline_ensemblvep_plugin/aaaac4cshvdayascvqjaadyaae\",
      \"id\": \"aaaac4cshvdayascvqjaadyaae\",
      \"workflow_instance_id\": \"PMCWI000AAI\",
      \"label\": \"ExAC\",
      \"file_id\": \"/hpc/pmc_kuiper/References/annotation/variants/grch38/ExAC.r0.3.1.sites.vep.vcf.gz\",
      \"index\": \"/hpc/pmc_kuiper/References/annotation/variants/grch38/ExAC.r0.3.1.sites.vep.vcf.gz.tbi\",
      \"filetype_id\": \"vcf\",
      \"fields\": \"ExAC_AC,ExAC_AN\",
      \"misc\": null
    },
    {
      \"href\": \"/api/v1/pipeline_ensemblvep_plugin/aaaac4cshvdmqascvqjaadyaaq\",
      \"id\": \"aaaac4cshvdmqascvqjaadyaaq\",
      \"workflow_instance_id\": \"PMCWI000AAI\",
      \"label\": \"dbNSFP\",
      \"file_id\": \"/hpc/pmc_kuiper/References/annotation/variants/grch38/dbNSFP3.5a.txt.gz\",
      \"index\": \"/hpc/pmc_kuiper/References/annotation/variants/grch38/dbNSFP3.5a.txt.gz.tbi\",
      \"misc\": \"/hpc/pmc_kuiper/References/annotation/variants/grch38/dbNSFP3.5a.readme.txt\",
      \"filetype_id\": \"vcf\",
      \"fields\": \"CADD_phred,gnomAD_exomes_AF,gnomAD_exomes_NFE_AF,ExAC_nonTCGA_AF,ExAC_nonTCGA_NFE_AF,gnomAD_genomes_AF,gnomAD_genomes_NFE_AF,phyloP100way_vertebrate,clinvar_rs,clinvar_clnsig,clinvar_trait,clinvar_golden_stars\"
    }
  ],
  \"GermlineSingleSample.vep_species\": \"Homo sapiens\",
  \"GermlineSingleSample.vep_scatter_count\": 20,
  \"GermlineSingleSample.vep_annotations\": [
    {
      \"href\": \"/api/v1/pipeline_ensemblvep_annotation/aaaac4cshvhzuascvqjaadyaae\",
      \"id\": \"aaaac4cshvhzuascvqjaadyaae\",
      \"workflow_instance_id\": \"PMCWI000AAI\",
      \"label\": \"Diag_Germline_Gene\",
      \"file_id\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/genome_targets/hg38_diagnostic_germline_2.1.bed.gz\",
      \"index\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/genome_targets/hg38_diagnostic_germline_2.1.bed.gz.tbi\",
      \"filetype_id\": \"bed\",
      \"annotationtype\": \"overlap\",
      \"reportcoordinates\": 0,
      \"fields\": \"\"
    }
  ],
  \"GermlineSingleSample.known_indels_sites_indices\": [
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.idx\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.known_indels.vcf.idx\"
  ],
  \"GermlineSingleSample.fingerprint_genotypes_file\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/empty.fingerprint.vcf\",
  \"GermlineSingleSample.molgenis_token\": \"\",
  \"GermlineSingleSample.gvcf_exp\": \"*.g.vcf.gz\",
  \"GermlineSingleSample.web_url\": \"\",
  \"GermlineSingleSample.omni_vcf_index\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/1000G_omni2.5.hg38.vcf.gz.tbi\",
  \"GermlineSingleSample.wgs_coverage_interval_list\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/wgs_coverage_regions.hg38.interval_list\",
  \"GermlineSingleSample.variants_for_contamination\": \"/hpc/pmc_kuiper/annotation/variants/resources/gnomad_hg38_exome.r2.1.AF_0.01-0.2.biallelic.vcf.gz\",
  \"GermlineSingleSample.variants_for_contamination_index\": \"/hpc/pmc_kuiper/annotation/variants/resources/gnomad_hg38_exome.r2.1.AF_0.01-0.2.biallelic.vcf.gz.tbi\",
  \"GermlineSingleSample.hapmap_vcf\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/hapmap_3.3.hg38.vcf.gz\",
  \"GermlineSingleSample.haplotype_database_file\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/empty.haplotype_map.txt\",
  \"GermlineSingleSample.wgs_calling_interval_list\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/wgs_calling_regions.hg38.interval_list\",
  \"GermlineSingleSample.wgs_evaluation_interval_list\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/wgs_evaluation_regions.hg38.interval_list\",
  \"GermlineSingleSample.hapmap_vcf_index\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/hapmap_3.3.hg38.vcf.gz.tbi\",
  \"GermlineSingleSample.scattered_calling_intervals\": [
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0001_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0002_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0003_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0004_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0005_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0006_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0007_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0008_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0009_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0010_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0011_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0012_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0013_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0014_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0015_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0016_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0017_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0018_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0019_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0020_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0021_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0022_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0023_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0024_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0025_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0026_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0027_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0028_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0029_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0030_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0031_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0032_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0033_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0034_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0035_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0036_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0037_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0038_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0039_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0040_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0041_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0042_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0043_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0044_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0045_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0046_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0047_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0048_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0049_of_50/scattered.interval_list\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0050_of_50/scattered.interval_list\"
  ],
  \"GermlineSingleSample.known_indels_sites_VCFs\": [
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf\",
    \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.known_indels.vcf\"
  ],
  \"GermlineSingleSample.ref_alt\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.alt\",
  \"GermlineSingleSample.ref_pac\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.pac\",
  \"GermlineSingleSample.CheckContamination.variants_for_contamination_index\": \"/hpc/pmc_gen/annotation/variants/resources/gnomad_hg38_exome.r2.1.AF_0.01-0.2.biallelic.vcf.gz.tbi\",
  \"GermlineSingleSample.multiqc_config_file\": \"/hpc/pmc_gen/configurations/multiqc/germline_snv_v1.3.0.yaml\",
  \"GermlineSingleSample.dbSNP_vcf_index\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dbsnp138.vcf.idx\",
  \"GermlineSingleSample.ref_bwt\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.bwt\",
  \"GermlineSingleSample.reference_version\": \"GRCh38\",
  \"GermlineSingleSample.ref_fasta\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta\",
  \"GermlineSingleSample.ref_sa\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.sa\",
  \"GermlineSingleSample.ref_ann\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.ann\",
  \"GermlineSingleSample.ref_fasta_index\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.fai\",
  \"GermlineSingleSample.ref_dict\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dict\",
  \"GermlineSingleSample.dbSNP_vcf\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dbsnp138.vcf\",
  \"GermlineSingleSample.ref_amb\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.amb\",
  \"GermlineSingleSample.omni_vcf\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/1000G_omni2.5.hg38.vcf.gz\",
  \"GermlineSingleSample.baitsBedFile\": \"$TARGET_INTERVALS_BED_FILE\",
  \"GermlineSingleSample.targetSomaticBedFile\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/genome_targets/hg38_diagnostic_somatic_2.0.bed\",
  \"GermlineSingleSample.targetGermlineBedFile\": \"/hpc/pmc_kuiper/References/hg38bundle/v0/genome_targets/hg38_diagnostic_germline_2.1.bed\",
  \"GermlineSingleSample.ValidateAggregatedSamFile.max_output\": null,
  \"GermlineSingleSample.ValidateReadGroupSamFile.max_output\": null,
  \"GermlineSingleSample.sub_strip_path\": \"/.*/\",
  \"GermlineSingleSample.ConvertToCram.cram_exp\": \"*.cra[im]\",
  \"GermlineSingleSample.FastQCreadgroup.html_exp\": \"*.html\",
  \"GermlineSingleSample.lowCoverageCutOff\": \"50\",
  \"GermlineSingleSample.ValidateAggregatedSamFile.ignore\": null,
  \"GermlineSingleSample.strip_url\": \"file://data/isi/p/pmc_research/omics\",
  \"GermlineSingleSample.ValidateReadGroupSamFile.ignore\": null,
  \"GermlineSingleSample.target_host\": \"hpct03\",
  \"GermlineSingleSample.unmapped_bam_suffix\": \".ubam\",
  \"GermlineSingleSample.program_group_name\": \" bwamem\",
  \"GermlineSingleSample.program_record\": \" bwamem\",
  \"GermlineSingleSample.molgenis_url\": \"\",
  \"GermlineSingleSample.FastQCreadgroup.zip_exp\": \"*.zip\",
  \"GermlineSingleSample.vcf_exp\": \"*.vcf.gz\",
  \"GermlineSingleSample.multiqc_command_line_config\": \"{ report_comment: This report was generated for the <code>Germline SNV</code> pipeline }\",
  \"GermlineSingleSample.replace_this_input_bam_header_id\": null,
  \"GermlineSingleSample.module_r_version\": \"R/3.2.4\",
  \"GermlineSingleSample.module_bwa_version\": \"bwa/0.7.13\",
  \"GermlineSingleSample.module_verifyBamID_version\": \"verifyBamID/1.1.13\",
  \"GermlineSingleSample.module_samtools_version\": \"samtools/1.3\",
  \"GermlineSingleSample.module_trecodetools_version\": \"trecode-tools/7.0.0\",
  \"GermlineSingleSample.module_picard_version\": \"picardtools/2.20.1\",
  \"GermlineSingleSample.module_multiqc_version\": \"multiqc-forked/1.7.dev0-1\",
  \"GermlineSingleSample.module_molgenistools_version\": \"molgenistools/5.2\",
  \"GermlineSingleSample.module_ensemblvep_version\": \"ensemblvep/92\",
  \"GermlineSingleSample.module_python_version\": \"python/3.6.1\",
  \"GermlineSingleSample.module_gatk4_version\": \"gatk/package-4.0.1.2-local\",
  \"GermlineSingleSample.module_gatk_version\": \"gatk/all\",
  \"GermlineSingleSample.module_fastqc_version\": \"fastqc/0.11.5\",
  \"GermlineSingleSample.module_bedtools_version\": \"bedtools/2.25.0\",
  \"GermlineSingleSample.module_petalink_version\": \"petalink/1.2.6p10-1\"
}
">$PARAMDIR/sample.$SAMPLE.json
    
    
    
    
    
    
done
