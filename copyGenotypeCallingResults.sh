

########################################################
##### USER PARAMETERS, change to what you need
### Hashes to copy
#declare -A my_array
my_array=( 385b6f32-4674-4a75-950f-c01eab9d674a \
17615f94-f0e4-4fe1-8489-74a8ac25f54b \
789cc2e0-d5e3-4263-8dae-aa213b87e06a \
)


### Project specific parameters
LIBRARY_STRATEGY="WGS"
PROJECTNAME="MRD4ALL"
CROMWELLDIR="/hpc/pmc_kuiper/fvandijk2/cromwell-executions/GenotypeCallingSingleSample/"
OUTPUTDIR="/hpc/pmc_kuiper/$PROJECTNAME/RESULTS/GenotypeCallingSingleSample/"
PROCESSEDDIR="/data/isi/p/pmc_research/pmc_kuiper/general/PROJECTS_ONGOING/$PROJECTNAME/"
PROCESSEDRESULTSDIR="$PROCESSEDDIR/RESULTS/"


#
###
#####
########################################################


mkdir -p "$PROCESSEDRESULTSDIR"


#Function to check if cachecopy directory is present
function checkCacheCopy()
{
    local dir=$1
    local ext=$2

    #Retrieve perl ( it somehow doesn't work from bash )
    perl=$( which perl )
    FIND=$(find ${dir} -name "*${ext}" | head -n1) #Find directory
    DIR=$( dirname $FIND )
    #NEWPATH=$( echo "$DIR" | sed "s/shard-[0-9]\+/shard*/g")
    #echo ${NEWPATH}
    echo $DIR
}


for HASH in "${my_array[@]}"
do
    echo -e -n "\n### Processing data for ...\n"
    echo -e -n "\n### HASH: $HASH\n"
    INPUTDIR="$CROMWELLDIR/$HASH/"
    
    #Do recursive find for cram file to check whether there is an additional cacheCopy directory in the path
    FIND=$(find $CROMWELLDIR/$HASH/call-CombineVCF/ -name "*${LIBRARY_STRATEGY}.vcf.gz")
    
    
    if [[ $FIND == *"cacheCopy"* ]] #If FIND result contains cacheCopy do ...
    then
        CACHECOPY="YES"
    else
        CACHECOPY="NO"
    fi


    ###Extract sample name
    ##
    if [[ "$CACHECOPY" == "NO" ]]
    then
        INPUTDIRBASE="$INPUTDIR/call-CombineVCF/"
    else
        INPUTDIRBASE="$INPUTDIR/call-CombineVCF/cacheCopy/"
    fi
        
    SAMPLE=$( basename $INPUTDIRBASE/execution/*${LIBRARY_STRATEGY}.vcf.gz _${LIBRARY_STRATEGY}.vcf.gz )
    echo -e -n "### SAMPLE: $SAMPLE\n"
    SAMPLEDIR="$OUTPUTDIR/$SAMPLE/"
    mkdir -p $SAMPLEDIR
    PROCESSEDRESULTSSAMPLEDIR="$PROCESSEDRESULTSDIR/GenotypeCallingSingleSample/$SAMPLE/"
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR
    
    ###GvcfCallingMetrics
    ##
    echo -e -n "\n\n\n### Copying: GvcfCallingMetrics\n\n"
    DIR="$INPUTDIR/call-CollectGvcfCallingMetrics/"
    EXT="metrics"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectGvcfCallingMetrics/
    rsync -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectGvcfCallingMetrics/
    
    
    ###CallingMetrics
    ##
    echo -e -n "\n\n\n### Copying: CallingMetrics\n\n"
    DIR="$INPUTDIR/call-CollectCallingMetrics/"
    EXT="metrics"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectCallingMetrics/
    rsync -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectCallingMetrics/
    
    
    ###VepAnnotationOutput
    ##
    #!    ########## merged VEP annotated VCF file in different folder structure in cromwell/WDL pipeline v6
    #Example: /hpc/pmc_kuiper/fvandijk2/cromwell-executions/GermlineSingleSample/27ae3284-d054-4de3-af05-84f9019cf8eb/call-ensemblVep/Vep.ensemblVep/3beed5ed-b90e-4001-b88d-80bfd5e981bb/call-MergeVCFs/execution/PMRBM000ALQ_WXS.vcf.gz
    echo -e -n "\n\n\n### Copying: VepAnnotationOutput\n\n"     
    DIR="$INPUTDIR/call-ensemblVep/Vep.ensemblVep/*/call*/"
    EXT="${LIBRARY_STRATEGY}.vcf.gz"
    BASE=$(checkCacheCopy "$DIR" "$EXT")

    mkdir -p $SAMPLEDIR/call-ensemblVep/
    rsync -avr $(readlink -e $BASE/*vcf*) $SAMPLEDIR/call-ensemblVep/
    
    
    ###MergeGVCFS
    ##
    echo -e -n "\n\n\n### Copying: MergeGVCFS\n\n"   
    mkdir -p $SAMPLEDIR/call-MergeGVCFS/
    #Check if exists, otherwise cachecopy might be needed
    if [ -s $INPUTDIR/call-MergeGVCFS/execution/${SAMPLE}_${LIBRARY_STRATEGY}.g.vcf.gz ]
    then
        rsync -avr $(readlink -e $INPUTDIR/call-MergeGVCFS/execution/*vcf*) $SAMPLEDIR/call-MergeGVCFS/
    elif [ -s $INPUTDIR/call-MergeGVCFS/cacheCopy/execution/${SAMPLE}_${LIBRARY_STRATEGY}.g.vcf.gz ]
    then
        rsync -avr $(readlink -e $INPUTDIR/call-MergeGVCFS/cacheCopy/execution/*vcf*) $SAMPLEDIR/call-MergeGVCFS/
    else
        echo "ERROR: Cannot find GVCF data for sample: $SAMPLE"
        exit 1
    fi


    ###Copy CombineVCF
    ##
    echo -e -n "\n\n\n### Copying: CombineVCF\n\n"
    DIR="$INPUTDIR/call-CombineVCF/"
    EXT="${LIBRARY_STRATEGY}.vcf.gz"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CombineVCF/
    rsync -avr $(readlink -e $BASE/*vcf*) $SAMPLEDIR/call-CombineVCF/


    #
    ##
    ### Copy other results to PROCESSED drive
    #Copy all meterics and stats to PROCESSED RESULTS
    echo -e -n "\n\n\n### Copying other result files to PROCESSED drive\n\n"  
    mkdir -p $PROCESSEDRESULTSDIR/GenotypeCallingSingleSample/
    rsync -avr $SAMPLEDIR $PROCESSEDRESULTSSAMPLEDIR/



    echo -e -n "\n### Finished copying all data for hash: $HASH\n\n\n"  
    
    
done

echo "Finished copying all data!"





