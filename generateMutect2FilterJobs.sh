
#Original code created by S. Lelieveld#

PROJECTDIR="/hpc/pmc_kuiper/<projectHomeDir>/"
INPUTDIR="$PROJECTDIR/ANALYSIS/mutect2/batch2/"
JOBDIR="$PROJECTDIR/CODE/jobs/filterMutect2/batch2/"
OUTPUTDIR="$PROJECTDIR/ANALYSIS/filterMutect2/batch2/"

mkdir -p "$JOBDIR"
mkdir -p "$OUTPUTDIR"

# set global varaiables
REF_GENOME="/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta"
NUM_CORES=4
NUM_THREADS=$(expr $NUM_CORES \* 2)


# VEP paths
DIR_CACHE="/hpc/pmc_kuiper/References/vep92/cache/"
FASTA_FILE="/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta"
PLUGIN_PATH="/hpc/pmc_kuiper/References/vep92/plugin/"
NUM_FORKS=4 # recommended by the VEP team to use 4 forks
NUM_CORES=4

# Java VM values
XMX="32g" #Gigabytes

#Loop over VCFs to create input file lists
for VCF in $(ls $INPUTDIR/*chr10.vcf)
do

SAMPLE=$( basename $VCF .chr10.vcf)

# merge stats files
stats_files=($(find $INPUTDIR -maxdepth 1 -name "*$SAMPLE*.vcf.stats" | sort -V ))
stats_files_input_line=$(for stats_file in ${stats_files[@]}; do printf "%s %s %s\n" "-stats" $stats_file "\\"; done)

# MERGE F1R2 stats
# Find the chromosome specific f1r2.tar.gz files
f1r2_files=($(find $INPUTDIR -maxdepth 1 -name "*$SAMPLE*-f1r2.tar.gz*" | sort -V ))
f1r2_files_input_line=$(for f1r2_file in ${f1r2_files[@]}; do printf "%s %s %s\n" "-I" $f1r2_file "\\"; done)

# Merge Vcfs 
vcf_files=($(find $INPUTDIR -maxdepth 1 -name "*$SAMPLE*.vcf" | sort -V ))
vcf_files_input_line=$(for vcf_file in ${vcf_files[@]}; do printf "%s %s %s\n" "-I" $vcf_file "\\"; done)


echo "
#$ -S /bin/bash
#$ -N filterMutect2.$SAMPLE.sh
#$ -l tmpspace=50G
#$ -l h_rt=08:00:00
#$ -l h_vmem=40G
#$ -pe threaded $NUM_CORES 
#$ -o $JOBDIR/filterMutect2.$SAMPLE.sh.out
#$ -e $JOBDIR/filterMutect2.$SAMPLE.sh.err
#$ -cwd

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"


set -e


# load the required modules
module load gatk/4.1.1.0
module load Java/1.8.0_60
module load ensemblvep/92 
module load samtools/1.3


# merge stats
java -d64 -Xmx$XMX -Djava.io.tmpdir=\$TMPDIR -jar \$GATK4 \\
MergeMutectStats \\
$stats_files_input_line
-O $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.vcf.stats

# learn read orientation model
java -d64 -Xmx$XMX -Djava.io.tmpdir=\$TMPDIR -jar \$GATK4 \\
LearnReadOrientationModel \\
$f1r2_files_input_line
-O $OUTPUTDIR/$SAMPLE-read-orientation-model.tar.gz

# merge the vcf files
java -d64 -Xmx$XMX -Djava.io.tmpdir=\$TMPDIR -jar \$GATK4 \\
GatherVcfs \\
$vcf_files_input_line
-O $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.vcf \\
--REFERENCE_SEQUENCE $REF_GENOME

# add the filter column to the merged vcf
java -Djava.io.tmpdir=\$TMPDIR -Xmx$XMX -jar \$GATK4 \\
FilterMutectCalls \\
-R $REF_GENOME \\
-V $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.vcf \\
-O $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.filterAnnotated.vcf \\
--ob-priors $OUTPUTDIR/$SAMPLE-read-orientation-model.tar.gz

# extract PASS variants using awk
awk -F \"\\\t\" '{if(\$0 ~ /^#/) print; \\
else if ( \$7 == \"PASS\") print}' \\
$OUTPUTDIR/$SAMPLE-merged-mutect2Calls.filterAnnotated.vcf \\
> $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.vcf

java -Xmx$XMX -Djava.io.tmpdir=\$TMPDIR -jar \$GATK4 \\
SelectVariants \\
-R $REF_GENOME \\
-V $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.vcf  \\
-O $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.snp.vcf  \\
--select-type-to-include SNP \\
--restrict-alleles-to BIALLELIC

# extract the indels from the PASS-filtered variants (--restrictAllelesTo BIALLELIC enforces that mulitallelic sites are excluded)

java -Xmx$XMX -Djava.io.tmpdir=\$TMPDIR -jar \$GATK4 \\
SelectVariants \\
-R $REF_GENOME \\
-V $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.vcf  \\
-O $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.indel.vcf  \\
--select-type-to-include INDEL \\
--restrict-alleles-to BIALLELIC

# extract the multi nucleotide substitutions ( di-nucleotide, tri-nucleotide, etc., substitutions)
# (--restrictAllelesTo BIALLELIC enforces that mulitallelic sites are excluded) 

java -Xmx$XMX -Djava.io.tmpdir=\$TMPDIR -jar \$GATK4 \\
SelectVariants \\
-R $REF_GENOME \\
-V $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.vcf  \\
-O $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.mnp.vcf  \\
--select-type-to-include MNP \\
--restrict-alleles-to BIALLELIC



# annotate vcf-file
flag_fields=\"--fields Location,Allele,VARIANT_CLASS,Gene,Feature,SYMBOL,CCDS,STRAND,IMPACT,Consequence,SIFT,PolyPhen,CADD_phred,EXON,DISTANCE,COSMIC_ID,COSMIC_CNT,AC,AN,CADD_phred,gnomAD_exomes_AF,gnomAD_exomes_NFE_AF,ExAC_nonTCGA_AF,ExAC_nonTCGA_NFE_AF,gnomAD_genomes_AF,gnomAD_genomes_NFE_AF,phyloP100way_vertebrate,clinvar_rs,clinvar_clnsig,clinvar_trait,clinvar_golden_stars,Diag_Germline_Gene\"

# annotate substitutions vcf-file
vep --offline \\
--cache \\
--fork $NUM_FORKS \\
--dir_cache $DIR_CACHE \\
--assembly GRCh38 \\
--input_file $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.snp.vcf \\
--output_file $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.snp.vepAnnotated.vcf \\
--force_overwrite \\
--fasta $REF_GENOME \\
--vcf \\
--pick \\
--pick_order tsl,appris,rank \\
--dir_plugins $PLUGIN_PATH \\
--custom /hpc/pmc_kuiper/References/goNL/hg38/multisample.parents_only.info_only.vcf.gz,goNL,vcf,exact,0,AC,AF,AN \\
--custom /hpc/pmc_kuiper/References/gnomAD/gnomadGenomesHg38/genomewide/gnomad.genomes.r2.1.sites.grch38.noVEP.vcf.gz,gnomADg,vcf,exact,0,AC,AF,AN,AF_afr,AF_amr,AF_asj,AF_eas,AF_fin,AF_nfe,AF_oth \\
--plugin CADD,/hpc/pmc_kuiper/References/cadd/hg38/whole_genome_SNVs.tsv.gz

# annotate indel vcf-file
vep --offline \\
--cache \\
--fork $NUM_FORKS \\
--dir_cache $DIR_CACHE \\
--assembly GRCh38 \\
--input_file $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.indel.vcf \\
--output_file $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.indel.vepAnnotated.vcf \\
--force_overwrite \\
--fasta $REF_GENOME \\
--vcf \\
--pick \\
--pick_order tsl,appris,rank \\
--dir_plugins $PLUGIN_PATH \\
--custom /hpc/pmc_kuiper/References/goNL/hg38/multisample.parents_only.info_only.vcf.gz,goNL,vcf,exact,0,AC,AF,AN \\
--custom /hpc/pmc_kuiper/References/gnomAD/gnomadGenomesHg38/genomewide/gnomad.genomes.r2.1.sites.grch38.noVEP.vcf.gz,gnomADg,vcf,exact,0,AC,AF,AN,AF_afr,AF_amr,AF_asj,AF_eas,AF_fin,AF_nfe,AF_oth \\
--plugin CADD,/hpc/pmc_kuiper/References/cadd/hg38/whole_genome_SNVs.tsv.gz

# annotate mnp vcf-file
vep --offline \\
--cache \\
--fork $NUM_FORKS \\
--dir_cache $DIR_CACHE \\
--assembly GRCh38 \\
--input_file $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.mnp.vcf \\
--output_file $OUTPUTDIR/$SAMPLE-merged-mutect2Calls.passFiltered.mnp.vepAnnotated.vcf \\
--force_overwrite \\
--fasta $REF_GENOME \\
--vcf \\
--pick \\
--pick_order tsl,appris,rank \\
--dir_plugins $PLUGIN_PATH \\
--custom /hpc/pmc_kuiper/References/goNL/hg38/multisample.parents_only.info_only.vcf.gz,goNL,vcf,exact,0,AC,AF,AN \\
--custom /hpc/pmc_kuiper/References/gnomAD/gnomadGenomesHg38/genomewide/gnomad.genomes.r2.1.sites.grch38.noVEP.vcf.gz,gnomADg,vcf,exact,0,AC,AF,AN,AF_afr,AF_amr,AF_asj,AF_eas,AF_fin,AF_nfe,AF_oth \\
--plugin CADD,/hpc/pmc_kuiper/References/cadd/hg38/whole_genome_SNVs.tsv.gz 


cd $OUTPUTDIR

bgzip $SAMPLE-merged-mutect2Calls.passFiltered.snp.vepAnnotated.vcf
bgzip $SAMPLE-merged-mutect2Calls.passFiltered.indel.vepAnnotated.vcf
bgzip $SAMPLE-merged-mutect2Calls.passFiltered.mnp.vepAnnotated.vcf

tabix $SAMPLE-merged-mutect2Calls.passFiltered.snp.vepAnnotated.vcf.gz
tabix $SAMPLE-merged-mutect2Calls.passFiltered.indel.vepAnnotated.vcf.gz
tabix $SAMPLE-merged-mutect2Calls.passFiltered.mnp.vepAnnotated.vcf.gz

md5sum $SAMPLE-merged-mutect2Calls.passFiltered.snp.vepAnnotated.vcf.gz > $SAMPLE-merged-mutect2Calls.passFiltered.snp.vepAnnotated.vcf.gz.md5
md5sum $SAMPLE-merged-mutect2Calls.passFiltered.indel.vepAnnotated.vcf.gz > $SAMPLE-merged-mutect2Calls.passFiltered.indel.vepAnnotated.vcf.gz.md5
md5sum $SAMPLE-merged-mutect2Calls.passFiltered.mnp.vepAnnotated.vcf.gz > $SAMPLE-merged-mutect2Calls.passFiltered.mnp.vepAnnotated.vcf.gz.md5

cd -

#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

">$JOBDIR/filterMutect2.$SAMPLE.sh

done
