

BATCHDIR="batch2/"
PROJECTDIR="/hpc/pmc_kuiper/MRD4ALL/"
PIPELINEDIR="$PROJECTDIR/CODE/pmc_kuiper_pipelines//wdl_pipeline_v2.1.0/wdl/"
INPUTDIR="$PROJECTDIR/CODE/runConfigs/GenotypeCallingSingleSample/$BATCHDIR/"


#Zip pipeline and dependencies
cd $PIPELINEDIR
zip workflowDependencies.zip \
cnv_common_tasks.wdl \
dataprep.wdl \
getVersion.wdl \
simpleChecks.wdl \
picard.wdl \
structureConversion.wdl \
align.wdl \
qc.wdl \
genomicIntervals.wdl \
gatk.wdl \
samtools.wdl \
bedTools.wdl \
ensemblVep.wdl \
map_keys.wdl \
dataconnections.wdl \
annotate.wdl \
molgenis.wdl
cd -



#Move dependencies zip to json directory
mv $PIPELINEDIR/workflowDependencies.zip $INPUTDIR

#Execute pipeline using cromwell
for JSON in $( ls $INPUTDIR/sample.*.json )
do

echo "$JSON"
    
curl -X POST "https://fvandijk2.hpccw.op.umcutrecht.nl/api/workflows/v1" \
  --user <USERNAME>:<PASSWORD> \
    --header "accept: application/json" \
    --header "Content-Type: multipart/form-data" \
    --form "workflowSource=@$PIPELINEDIR/genotype_calling_single_sample_workflow.wdl" \
    --form "workflowInputs=@$JSON" \
    --form "workflowDependencies=@$INPUTDIR/workflowDependencies.zip"

echo ""

done

#Check progress
#https://fvandijk2.hpccw.op.umcutrecht.nl/api/workflows/v2/3e95119a-5b74-45ab-b00a-58a96c069bdb/status



#curl -X POST "https://fvandijk2.hpccw.op.umcutrecht.nl/api/workflows/v1/e1cd5a81-179a-4690-b225-d8e8b24222f3/abort" --user fvandijk2:<PASSWORD>






