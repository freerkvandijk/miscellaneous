
#File containing TUMOR/NORMAL pairs, use sample IDs
##EXAMPLE (TUMORID tab NORMALID)
#MY_TUMOR_SAMPLE_ID MY_NORMAL_SAMPLE_ID
TUMORNORMALFILE="/hpc/pmc_kuiper/<PROJECTNAME>/CODE/TumorNormal.batch1.txt"

#CRAM directory containing input data
CRAMDIR="/hpc/pmc_kuiper/<PROJECTNAME>/RESULTS/GermlineCalling/*/call-ConvertToCram/"
OUTPUTDIR="/hpc/pmc_kuiper/<PROJECTNAME>/CODE/jobs/scnv_single_sample_inputs_no_molgenis/"

#PANEL of normals
PANELOFNORMALS="/hpc/pmc_kuiper/References/panelOfNormals/WGS/BladderCancer_PoN/BladderCancer_PoN_21samples.pon.hdf5"
PANELOFNORMALSMD5="06d654d4a4120719c76eb34377c35947"

mkdir -p $OUTPUTDIR


#Iterate over all CRAM files and extract samplename and md5sum, also generate date for analysis
while read line
do

    TUMOR=$( echo $line | awk '{print $1}' )
    NORMAL=$( echo $line | awk '{print $2}' )
    
    TUMORCRAM=$( ls $CRAMDIR/$TUMOR*.cram )
    NORMALCRAM=$( ls $CRAMDIR/$NORMAL*.cram )
    
    TUMORCRAMMD5=$( head -n1 $TUMORCRAM.md5 | awk '{print $1}' )
    NORMALCRAMMD5=$( head -n1 $NORMALCRAM.md5 | awk '{print $1}' )
    

    DATE=$(date --iso-8601=seconds)

OUTPUTJSON="$OUTPUTDIR/scnv_single_sample_inputs_no_molgenis.$TUMOR.$NORMAL.json"

#Echo first part of json
echo "
{
   \"CNVSomaticPairWorkflow.tumor_normal_pair\" : {" > $OUTPUTJSON


#Echo second part of json, containing all samples to process
echo "\"tumor_cram\" : \"$TUMORCRAM\",
      \"tumor_cram_md5\" : \"$TUMORCRAMMD5\",
      \"tumor_label\" : \"$TUMOR\",
      \"tumor_id\" : \"$TUMOR\",
      \"normal_cram\" : \"$NORMALCRAM\",
      \"normal_cram_md5\" : \"$NORMALCRAMMD5\",
      \"normal_label\" : \"$NORMAL\",
      \"normal_id\" : \"$NORMAL\"," >> $OUTPUTJSON

#Echo third part of json, containing all variables
echo "},
   \"CNVSomaticPairWorkflow.web_host\" : \"\",   
   \"CNVSomaticPairWorkflow.num_samples_allele_fraction\" : null,
   \"CNVSomaticPairWorkflow.module_autossh_version\" : \"autossh/1.4e\",
   \"CNVSomaticPairWorkflow.data_host\" : \"hpct01\",
   \"CNVSomaticPairWorkflow.kernel_variance_allele_fraction\" : null,
   \"CNVSomaticPairWorkflow.neutral_segment_copy_ratio_upper_bound\" : null,
   \"CNVSomaticPairWorkflow.smoothing_threshold_allele_fraction\" : null,
   \"CNVSomaticPairWorkflow.outlier_neutral_segment_copy_ratio_z_score_threshold\" : null,
   \"CNVSomaticPairWorkflow.genotyping_base_error_rate\" : null,
   \"CNVSomaticPairWorkflow.smoothing_threshold_copy_ratio\" : null,
   \"CNVSomaticPairWorkflow.module_samtools_version\" : \"samtools/1.3\",
   \"CNVSomaticPairWorkflow.molgenis_token\" : \"\",
   \"CNVSomaticPairWorkflow.library_strategy\" : \"WholeGenomeSequencing\",
   \"CNVSomaticPairWorkflow.neutral_segment_copy_ratio_lower_bound\" : null,
   \"CNVSomaticPairWorkflow.read_count_pon\" : \"$PANELOFNORMALS\",
   \"CNVSomaticPairWorkflow.read_count_pon_md5\" : \"$PANELOFNORMALSMD5\",
   \"CNVSomaticPairWorkflow.workflow_id\" : \"somatic_cnv\",
   \"CNVSomaticPairWorkflow.submission_date\" : \"$DATE\",
   \"CNVSomaticPairWorkflow.module_r_version\" : \"R/3.6.1\",
   \"CNVSomaticPairWorkflow.study\" : \"CNV_calling\",
   \"CNVSomaticPairWorkflow.bin_length\" : \"1000\",

   \"CNVSomaticPairWorkflow.kernel_scaling_allele_fraction\" : null,
   \"CNVSomaticPairWorkflow.format\" : null,
   \"CNVSomaticPairWorkflow.ModelSegmentsNormal.normal_allelic_counts\" : null,
   \"CNVSomaticPairWorkflow.kernel_variance_copy_ratio\" : null,
   \"CNVSomaticPairWorkflow.calling_copy_ratio_z_score_threshold\" : null,
   \"CNVSomaticPairWorkflow.tmpspace_gb_moderate\" : \"40\",
   \"CNVSomaticPairWorkflow.min_total_allele_count\" : null,
   \"CNVSomaticPairWorkflow.minimum_contig_length\" : \"46000000\",
   \"CNVSomaticPairWorkflow.wallclock_short\" : \"2:00:00\",
   \"CNVSomaticPairWorkflow.minor_allele_fraction_prior_alpha\" : null,
   \"CNVSomaticPairWorkflow.experiment_ids\" : [
      \"exp1\",
      \"exp2\"
   ],
   \"CNVSomaticPairWorkflow.num_smoothing_iterations_per_fit\" : null,
   \"CNVSomaticPairWorkflow.intervals\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/wgs_calling_regions.hg38.interval_list\",
   \"CNVSomaticPairWorkflow.target_host\" : \"hpct01\",
   \"CNVSomaticPairWorkflow.ref_dict\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dict\",
   \"CNVSomaticPairWorkflow.number_of_eigensamples\" : null,
   \"CNVSomaticPairWorkflow.num_burn_in_copy_ratio\" : null,
   \"CNVSomaticPairWorkflow.module_molgenistools_version\" : \"molgenistools/4\",
   \"CNVSomaticPairWorkflow.experiment_type\" : \"WholeGenomeSequencing\",
   \"CNVSomaticPairWorkflow.num_changepoints_penalty_factor\" : null,
   \"CNVSomaticPairWorkflow.max_num_smoothing_iterations\" : null,
   \"CNVSomaticPairWorkflow.kernel_approximation_dimension\" : null,
   \"CNVSomaticPairWorkflow.module_gatk4_version\" : \"gatk/package-4.0.1.2-local\",
   \"CNVSomaticPairWorkflow.max_num_segments_per_chromosome\" : null,
   \"CNVSomaticPairWorkflow.padding\" : \"250\",
   \"CNVSomaticPairWorkflow.ref_fasta_index\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.fai\",
   \"CNVSomaticPairWorkflow.strip_url\" : \"file:///.*:\",
   \"CNVSomaticPairWorkflow.cram_suffix\" : \".cram\",
   \"CNVSomaticPairWorkflow.ref_fasta\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta\",
   \"CNVSomaticPairWorkflow.minimum_base_quality\" : null,
   \"CNVSomaticPairWorkflow.wallclock_moderate\" : \"8:00:00\",
   \"CNVSomaticPairWorkflow.num_burn_in_allele_fraction\" : null,
   \"CNVSomaticPairWorkflow.genotyping_homozygous_log_ratio_threshold\" : null,
   \"CNVSomaticPairWorkflow.molgenis_host\" : \"\",
   \"CNVSomaticPairWorkflow.num_samples_copy_ratio\" : null,
   \"CNVSomaticPairWorkflow.output_dir\" : null,
   \"CNVSomaticPairWorkflow.analysis_ids\" : [
      \"exp1\"
   ],
   \"CNVSomaticPairWorkflow.common_sites\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/common_sites_1kg.phase3.v5a.snp.maf10.biallelic.hg38.interval_list\",
   \"CNVSomaticPairWorkflow.file_id_exp\" : \"[a-z0-9]{32}\",
   \"CNVSomaticPairWorkflow.tmpspace_gb_small\" : \"4\",
   \"CNVSomaticPairWorkflow.wallclock_ages\" : \"48:00:00\",
   \"CNVSomaticPairWorkflow.derived_data_path\" : \"\",
   \"CNVSomaticPairWorkflow.wallclock_long\" : \"24:00:00\",
   \"CNVSomaticPairWorkflow.sub_strip_path\" : \"/.*/\"
}" >> $OUTPUTJSON

done<$TUMORNORMALFILE
