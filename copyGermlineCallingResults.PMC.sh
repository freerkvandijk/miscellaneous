

########################################################
##### USER PARAMETERS, change to what you need
### Samples to copy
#declare -A my_array
my_array=( KR04601-BL13 KR04601-GL2 \
KR04601-TL14 \
)

### Project specific parameters
LIBRARY_STRATEGY="WGS"
PROJECTNAME="DNARepairAndCongenitalImmunodeficiencySyndromes"
INPUTDIR="/hpc/pmc_kuiper/$PROJECTNAME/RESULTS/GermlineCalling/" #path to RESULTS directory on HPC
PROCESSEDDIR="/data/groups/pmc_kuiper/general/PROJECTS_ONGOING/$PROJECTNAME/"
PROCESSEDCRAMDIR="$PROCESSEDDIR/DATA/CRAM/"
PROCESSEDRESULTSDIR="$PROCESSEDDIR/RESULTS/"
USERNAME="fvandijk2"
HOST="hpct03.op.umcutrecht.nl"
RSYNC="${USERNAME}@${HOST}"
CONNECTION="ssh ${USERNAME}@${HOST} source ~/.bashrc;"

#
###
#####
########################################################


mkdir -p "$PROCESSEDCRAMDIR"
mkdir -p "$PROCESSEDRESULTSDIR"


for SAMPLE in "${my_array[@]}"
do
    echo -e -n "\n### Processing data for ...\n"
    echo -e -n "\n### SAMPLE: $SAMPLE\n"

    INPUTSAMPLEDIR="$INPUTDIR/$SAMPLE/"
    PROCESSEDRESULTSSAMPLEDIR="$PROCESSEDRESULTSDIR/GermlineSingleSample/$SAMPLE/"
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR
    
    
    ###QualityYieldMetrics
    ##
    echo -e -n "\n\n\n### Copying: QualityYieldMetrics\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-CollectQualityYieldMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectQualityYieldMetrics/*.unmapped.quality_yield_metrics $PROCESSEDRESULTSSAMPLEDIR/call-CollectQualityYieldMetrics/
    
    
    ###UnsortedReadgroupBamQualityMetrics
    ##
    echo -e -n "\n\n\n### Copying: UnsortedReadgroupBamQualityMetrics\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/*.pdf $PROCESSEDRESULTSSAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/*metrics $PROCESSEDRESULTSSAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    
    
    ###MarkDuplicates
    ##
    echo -e -n "\n\n\n### Copying: MarkDuplicates\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-MarkDuplicates/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-MarkDuplicates/*metrics $PROCESSEDRESULTSSAMPLEDIR/call-MarkDuplicates/
    

    ###AggregationMetrics
    ##
    echo -e -n "\n\n\n### Copying: AggregationMetrics\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-CollectAggregationMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectAggregationMetrics/*.pdf $PROCESSEDRESULTSSAMPLEDIR/call-CollectAggregationMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectAggregationMetrics/*metrics $PROCESSEDRESULTSSAMPLEDIR/call-CollectAggregationMetrics/
    
    
    ###WgsMetrics
    ##
    echo -e -n "\n\n\n### Copying: WgsMetrics\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-CollectWgsMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectWgsMetrics/*metrics $PROCESSEDRESULTSSAMPLEDIR/call-CollectWgsMetrics/
    
    
    ###ReadgroupBamQualityMetrics
    ##
    echo -e -n "\n\n\n### Copying: ReadgroupBamQualityMetrics\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectReadgroupBamQualityMetrics/*.pdf $PROCESSEDRESULTSSAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectReadgroupBamQualityMetrics/*metrics $PROCESSEDRESULTSSAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    
    ###RawWgsMetrics
    ##
    echo -e -n "\n\n\n### Copying: RawWgsMetrics\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-CollectRawWgsMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectRawWgsMetrics/*metrics $PROCESSEDRESULTSSAMPLEDIR/call-CollectRawWgsMetrics/
    
    
    ###GvcfCallingMetrics
    ##
    echo -e -n "\n\n\n### Copying: GvcfCallingMetrics\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-CollectGvcfCallingMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectGvcfCallingMetrics/*metrics $PROCESSEDRESULTSSAMPLEDIR/call-CollectGvcfCallingMetrics/
    
    ###CallingMetrics
    ##
    echo -e -n "\n\n\n### Copying: CallingMetrics\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-CollectCallingMetrics/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-CollectCallingMetrics/*metrics $PROCESSEDRESULTSSAMPLEDIR/call-CollectCallingMetrics/
    
    
    ###GatherQCreports
    ##
    echo -e -n "\n\n\n### Copying: GatherQCreports\n\n"
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-gatherQCreports/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-gatherQCreports/*multiqc* $PROCESSEDRESULTSSAMPLEDIR/call-gatherQCreports/
    
    ###VepAnnotationOutput
    ##
    echo -e -n "\n\n\n### Copying: VepAnnotationOutput\n\n"     

    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-ensemblVep/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-ensemblVep/*vcf* $PROCESSEDRESULTSSAMPLEDIR/call-ensemblVep/
    
    
    ###MergeGVCFS
    ##
    echo -e -n "\n\n\n### Copying: MergeGVCFS\n\n"   
    
    mkdir -p $PROCESSEDRESULTSSAMPLEDIR/call-MergeGVCFS/
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-MergeGVCFS/*vcf* $PROCESSEDRESULTSSAMPLEDIR/call-MergeGVCFS/
        
    #
    ##
    ### Copy CRAM file to PROCESSED drive
    #Check if CRAM file md5sum file exists and is not empty
    echo -e -n "\n\n\n### Copying CRAM file to PROCESSED drive\n\n"
    
    rsync -vr --copy-links --progress ${RSYNC}:$INPUTSAMPLEDIR/call-ConvertToCram/*.cra* $PROCESSEDCRAMDIR/


    #Echo done for this sample
    echo -e -n "\n### Finished copying all data for sample: $SAMPLE\n\n\n"  
    
    
done

echo "Finished copying all data!"









