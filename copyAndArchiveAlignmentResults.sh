

#declare -A my_array

my_array=( 8ecad97e-dfee-4e92-a598-f31310aedc1d 3a464f99-9838-4e29-b921-91cefbb009bc 67913400-217e-4a40-9501-474f71103c94 )

CROMWELLDIR="/<pathToHomeDir>/cromwell-executions/AlignmentSingleSample/"
OUTPUTDIR="/<projectResultDir>/RESULTS/AlignmentSingleSample/"
PROCESSEDDIR="/<pathToProjectDirOnProcessedDrive>/"
PROCESSEDCRAMDIR="$PROCESSEDDIR/DATA/CRAM/"
PROCESSEDRESULTSDIR="$PROCESSEDDIR/RESULTS/"

for HASH in "${my_array[@]}"
do
    echo "$HASH"
    INPUTDIR="$CROMWELLDIR/$HASH/"
    SAMPLE=$( basename $CROMWELLDIR/$HASH/call-ConvertToCram/execution/*WGS.cram _WGS.cram )
    SAMPLEDIR="$OUTPUTDIR/$SAMPLE/"
    mkdir -p $SAMPLEDIR
    
    #
    ##
    ###Copy all metrics and statistics to RESULTS and PROCESSED
    ##
    #
    
    #QualityYieldMetrics
    mkdir -p $SAMPLEDIR/call-CollectQualityYieldMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectQualityYieldMetrics/*/execution/*.unmapped.quality_yield_metrics) $SAMPLEDIR/call-CollectQualityYieldMetrics/
    
    #UnsortedReadgroupBamQualityMetrics
    mkdir -p $SAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectUnsortedReadgroupBamQualityMetrics/*/execution/*.pdf) $SAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectUnsortedReadgroupBamQualityMetrics/*/execution/*metrics) $SAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    
    #MarkDuplicates
    mkdir -p $SAMPLEDIR/call-MarkDuplicates/
    cp -av $(readlink -e $INPUTDIR/call-MarkDuplicates/execution/*metrics) $SAMPLEDIR/call-MarkDuplicates/
    
    #AggregationMetrics
    mkdir -p $SAMPLEDIR/call-CollectAggregationMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectAggregationMetrics/execution/*.pdf) $SAMPLEDIR/call-CollectAggregationMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectAggregationMetrics/execution/*metrics) $SAMPLEDIR/call-CollectAggregationMetrics/
    
    #WgsMetrics
    mkdir -p $SAMPLEDIR/call-CollectWgsMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectWgsMetrics/execution/*metrics) $SAMPLEDIR/call-CollectWgsMetrics/
    
    #ReadgroupBamQualityMetrics
    mkdir -p $SAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectReadgroupBamQualityMetrics/execution/*.pdf) $SAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectReadgroupBamQualityMetrics/execution/*metrics) $SAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    
    #RawWgsMetrics
    mkdir -p $SAMPLEDIR/call-CollectRawWgsMetrics/
    cp -av $(readlink -e $INPUTDIR/call-CollectRawWgsMetrics/execution/*metrics) $SAMPLEDIR/call-CollectRawWgsMetrics/
    
    #GatherQCreports
    mkdir -p $SAMPLEDIR/call-gatherQCreports/
    cp -av $(readlink -e $INPUTDIR/call-gatherQCreports/execution/*multiqc*) $SAMPLEDIR/call-gatherQCreports/
    
    #
    ##
    ###Copy all "big" output files to RESULTS and PROCESSED
    ##
    #
    
    #Copy all meterics and stats to PROCESSED RESULTS
    mkdir -p $PROCESSEDRESULTSDIR/AlignmentSingleSample/
    cp -av $SAMPLEDIR $PROCESSEDRESULTSDIR/AlignmentSingleSample/
    
    #Copy ConvertToCram to RESULTS on HPC
    mkdir -p $SAMPLEDIR/call-ConvertToCram/
    
    #Check if CRAM file md5sum file exists and is not empty
    if [ -s $CROMWELLDIR/$HASH/call-ConvertToCram/execution/$SAMPLE\_WGS.cram.md5 ]
    then
        echo "MD5SUM file exists: $CROMWELLDIR/$HASH/call-ConvertToCram/execution/$SAMPLE\_WGS.cram.md5"
    else
        echo "MD5SUM file does not exist: $CROMWELLDIR/$HASH/call-ConvertToCram/execution/$SAMPLE\_WGS.cram.md5"
        echo "Generating it now ..";
        cd $INPUTDIR/call-ConvertToCram/execution/
        md5sum $SAMPLE\_WGS.cram > $SAMPLE\_WGS.cram.md5
        cd -
    fi
    
    cp -av $(readlink -e $INPUTDIR/call-ConvertToCram/execution/*.cram) $SAMPLEDIR/call-ConvertToCram/
    cp -av $(readlink -e $INPUTDIR/call-ConvertToCram/execution/*.crai) $SAMPLEDIR/call-ConvertToCram/
    cp -av $(readlink -e $INPUTDIR/call-ConvertToCram/execution/*.cram.md5) $SAMPLEDIR/call-ConvertToCram/
    
    #Copy ConvertCram from HPC RESULTS to PROCESSED CRAM data dir
    cp -av $(readlink -e $SAMPLEDIR/call-ConvertToCram/*.cra*) $PROCESSEDCRAMDIR
    
    #Copy BAM file to RESULTS on HPC, use as input for SNV-analysis etc.
    mkdir -p $SAMPLEDIR/call-GatherBamFiles/
    
    #Check if CRAM file md5sum file exists and is not empty
    if [ -s $INPUTDIR/call-GatherBamFiles/execution/$SAMPLE\_WGS.bam.md5 ]
    then
        echo "MD5SUM file exists: $INPUTDIR/call-GatherBamFiles/execution/$SAMPLE\_WGS.bam.md5"
    else
        echo "MD5SUM file does not exist: $INPUTDIR/call-GatherBamFiles/execution/$SAMPLE\_WGS.bam.md5"
        echo "Generating it now ..";
        cd $INPUTDIR/call-GatherBamFiles/execution/
        md5sum $SAMPLE\_WGS.bam > $SAMPLE\_WGS.bam.md5
        cd -
    fi
    
    cp -av $(readlink -e $INPUTDIR/call-GatherBamFiles/execution/*WGS.bam) $SAMPLEDIR/call-GatherBamFiles/
    cp -av $(readlink -e $INPUTDIR/call-GatherBamFiles/execution/*WGS.bai) $SAMPLEDIR/call-GatherBamFiles/
    cp -av $(readlink -e $INPUTDIR/call-GatherBamFiles/execution/*WGS.bam.md5) $SAMPLEDIR/call-GatherBamFiles/


done
