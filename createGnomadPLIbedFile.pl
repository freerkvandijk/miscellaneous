#!/usr/bin/perl -w
use strict;
use warnings;
use diagnostics;
use List::Util qw(min max);
use List::Util qw(sum);
use List::Util qw(first);
use List::MoreUtils qw/ uniq /;
use File::Glob ':glob';
use File::Basename;
use Getopt::Long;
use POSIX qw(floor);

#This script uses a gnomad v2.1.1 lof metrics file to create a hg38 coordinate bases pLiBED file

##Read GTF file
print "Processing GTF file..\n";
open(GTF, "< /hpc/pmc_kuiper/References/annotation/genes/Homo_sapiens.GRCh38.95.gtf") || die "Can't open GTF file\n";
my %geneName2geneID;
my %geneID2geneName;
my $geneCount=0;
my %geneChr;
my %geneLength;
my %geneStart;
my %geneStop;
my %chrStart;
my %geneChrStartStop;
while (my $line=<GTF>){ #Read GTF file line by line
    chomp($line);
    if ($line !~ m/^#.+/gs) { #If not header line process further
        my @array = split("\t", $line);
        my $chr = "chr" . $array[0];
        #$chr =~ s/X/23/gs;
        #$chr =~ s/Y/24/gs;
        #$chr =~ s/MT/25/gs;
        my $feature = $array[2];
        my $start = $array[3];
        my $stop = $array[4];
        my $info = $array[8];
        my $geneID = "NA";
        my $gene = "NA";
        my $geneLength = ($stop-$start);
        if ($feature eq "gene") {
            #if (looks_like_number($chr)) { #Check if chromosome is a number
            
            #gene_id "ENSG00000223972"; gene_version "5"; gene_name "DDX11L1"; gene_source "havana"; gene_biotype "transcribed_unprocessed_pseudogene";
                if ($info =~ m/gene_id "(ENSG[0-9]{1,})"; gene_version.+; gene_name "(.+)"; gene_sourc.+/gs) { #Extract Ensembl gene ID
                    $geneID = $1;
                    $gene = $2;
                    $gene =~ s/(?>\x0D\x0A?|[\x0A-\x0C\x85\x{2028}\x{2029}])//;
                    $geneID =~ s/(?>\x0D\x0A?|[\x0A-\x0C\x85\x{2028}\x{2029}])//;
                    $geneName2geneID{ $gene } = $geneID;
                    $geneID2geneName{ $geneID } = $gene;
                    $geneChr{ $geneID } = $chr;
                    $geneLength{ $geneID } = $geneLength;
                    $geneStart{ $geneID } = $start;
                    $geneStop{ $geneID } = $stop;
                    $chrStart{ $chr }{ $start } = $geneID;
                    $geneCount++;
                    $geneChrStartStop{ $chr }{ $start } = $stop;
                }
            #}
        }
    }
}
close(GTF);
print "#Genes detected: $geneCount\n";
print "Done processing GTF file.\n\n";


##Process *.dict file to retrieve correct chromosome order
my @sortedChrs;
open(DICT, "< /hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dict") || die "Can't open DICT file\n";
while (my $lin = <DICT>){
    chomp($lin);
    if ($lin =~ m/\@SQ\tSN:(.+)\tLN:.+/gs) { #If not header line process further
        my @array = split("\t", $lin);
        my $chr = $1;
        push(@sortedChrs, $chr);
    }
}
close(DICT);


##Read gnomad lof metrics file
open(MET, "< /hpc/pmc_kuiper/References/vep92/pluginReferences/gnomad.v2.1.1.lof_metrics.by_gene.txt") || die "Can't open MET file\n";
my @met = <MET>;
close(MET);
my %geneChrStartPli;
for (my $i=1; $i<= $#met; $i++){
    my $line = $met[$i];
    chomp($line);
    my @array = split("\t", $line);
    my $geneName = $array[0];
    my $pLi = $array[20];
    if (exists $geneName2geneID{ $geneName }){ #Retrieve geneID
        my $geneID = $geneName2geneID{ $geneName };
        my $chr = $geneChr{ $geneID };
        my $start = $geneStart{ $geneID };
        my $stop = $geneStop{ $geneID };
        my $bedStart = ($start-1); #Do start position -1, because of BED offset
        my $val = "$chr\t$bedStart\t$stop\t$pLi";
        $geneChrStartPli{ $chr }{ $start } = $val;
    }
}


#Create output BED file
print "\nCreating output BED file..\n";
open(OUTPUT, "> /hpc/pmc_kuiper/References/vep92/pluginReferences/gnomad.v2.1.1.hg38coordinates.pLIscores.bed") || die "Can't open MET file\n";
foreach my $chrom (@sortedChrs){
    if (exists $geneChrStartPli{ $chrom }){
        #Sort start positions
        foreach my $start (sort {$a <=> $b} keys %{ $geneChrStartPli{ $chrom } }){
            my $val = $geneChrStartPli{ $chrom }{ $start };
            print OUTPUT "$val\n";
        }
    }
}
close(OUTPUT);
print "Done creating output BED file.\n\n";


