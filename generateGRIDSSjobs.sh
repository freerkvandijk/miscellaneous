####
SCHEDULER="SLURM"

#### Parameters to set
BATCHDIR="batch5"
PROJECTDIR="/hpc/pmc_kuiper/DNARepairAndCongenitalImmunodeficiencySyndromes/"
JOBDIR="$PROJECTDIR/CODE/jobs/GRIDSS/$BATCHDIR/"
OUTPUTDIR="$PROJECTDIR/ANALYSIS/GRIDSS/$BATCHDIR/"

SAMPLEFILE="$PROJECTDIR/samplelists/sampleListMutect2.$BATCHDIR.Converted.SinglePairsPerLine.txt"

####
#EXPERIMENT_TYPE="Whole genome sequencing"
EXPERIMENT_TYPE="Whole genome sequencing"
#LIBRARY_STRATEGY="WGS"
LIBRARY_STRATEGY="WGS"

####
GPLDIRECTORY="/hpc/local/CentOS7/pmc_kuiper/software/gridss-purple-linx/gridss-purple-linx-v1.2.0/"
PERLMODULESDIR="/hpc/local/CentOS7/pmc_kuiper/software/perl_modules/"
IMAGEMAGICKLIBDIR="/hpc/local/CentOS7/pmc_kuiper/software/ImageMagick-7.0.10-0/lib/"

DATE=$(date --iso-8601=seconds)

mkdir -p $JOBDIR
mkdir -p $OUTPUTDIR

while read line
do

	NORMAL=$( echo $line | awk '{print $1}')
	TUMOR=$( echo $line | awk '{print $2}')

	TUMORBAM="$PROJECTDIR/RESULTS/GermlineCalling/$TUMOR/call-GatherBamFiles/$TUMOR\_$LIBRARY_STRATEGY.bam"
	NORMALBAM="$PROJECTDIR/RESULTS/GermlineCalling/$NORMAL/call-GatherBamFiles/$NORMAL\_$LIBRARY_STRATEGY.bam"

    SAMPLE="$TUMOR.$NORMAL"
    OUTPUTSAMPLEDIR="$OUTPUTDIR/$SAMPLE/"
    mkdir -p "$OUTPUTSAMPLEDIR"
    
    
    echo "Generating job for sample: $SAMPLE"
  

##########
####CREATE JOB HEADER

JOBNAME="sample.$SAMPLE.GRIDSS.sh"
OUTPUTLOG="$JOBDIR/sample.$SAMPLE.GRIDSS.sh.out"
ERRORLOG="$JOBDIR/sample.$SAMPLE.GRIDSS.sh.err"
WALLTIME="19:59:00"
NUMTASKS="1"
NUMCPUS="20"
MEM="30G"
TMPSPACE="800G"
JOBFILE="$JOBDIR/sample.$SAMPLE.GRIDSS.sh"

if [[ $SCHEDULER == "SLURM" ]]
then
    cat <<- EOF > $JOBFILE
#!/bin/bash
#SBATCH --job-name=$JOBNAME
#SBATCH --output=$OUTPUTLOG
#SBATCH --error=$ERRORLOG
#SBATCH --partition=cpu
#SBATCH --time=$WALLTIME
#SBATCH --ntasks=$NUMTASKS
#SBATCH --cpus-per-task $NUMCPUS
#SBATCH --mem=$MEM
#SBATCH --gres=tmpspace:$TMPSPACE
#SBATCH --nodes=1
#SBATCH --open-mode=append

EOF

elif [[ $SCHEDULER == "SGE" ]]
then

    cat <<- EOF > $JOBFILE
#$ -S /bin/bash
#$ -N $JOBNAME
#$ -o $OUTPUTLOG
#$ -e $ERRORLOG
#$ -l h_rt=$WALLTIME
#$ -l h_vmem=$MEM
#$ -l tmpspace=$TMPSPACE
#$ -cwd


EOF

else
    echo "Type of scheduler not known: $SCHEDULER"
    exit
fi


    
echo "


set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"


#Set environment variables
GRIDSS_PURPLE_LINX_SH=\"$GPLDIRECTORY/gridss-purple-linx/gridss-purple-linx_hg38.sh\"

export GRIDSS_JAR=\"$GPLDIRECTORY/gridss/gridss-2.7.3-gridss-jar-with-dependencies.jar\"
export AMBER_JAR=\"$GPLDIRECTORY/hmftools/amber-3.1.jar\"
export COBALT_JAR=\"$GPLDIRECTORY/hmftools/cobalt-1.8.jar\"
export PURPLE_JAR=\"$GPLDIRECTORY/hmftools/purple-2.36.jar\"
export LINX_JAR=\"$GPLDIRECTORY/hmftools/sv-linx_1.7.jar\"

export PERL5LIB=\"$PERLMODULESDIR\"
export LD_LIBRARY_PATH=\"$IMAGEMAGICKLIBDIR\":\$LD_LIBRARY_PATH


#Load modules
module load bwa/0.7.13
module load sambamba/0.6.0
module load circos/0.69-9
module load gridss/2.7.3
module load picardtools/2.18.22
module load R/3.6.1


cd ${OUTPUTSAMPLEDIR}

#Run GRIDSS
#Default JVM heapspace is 25G
bash \${GRIDSS_PURPLE_LINX_SH} \\
-t ${TUMORBAM} \\
-n ${NORMALBAM} \\
-s ${SAMPLE} \\
--threads ${NUMCPUS}


#Rerun last Linx command to generate clean circos plots
#by default _T gets added to tumour sample name, add it like this, so no backslashes are in the samplename
tumour_sample=\"${SAMPLE}_T\"
run_dir=\"$OUTPUTSAMPLEDIR\"

java -cp \$LINX_JAR com.hartwig.hmftools.linx.visualiser.SvVisualiser \\
-sample \$tumour_sample \\
-plot_out \$run_dir/linx/plot/ \\
-data_out \$run_dir/linx/circos/ \\
-segment \$run_dir/linx/\$tumour_sample.linx.vis_segments.tsv \\
-link \$run_dir/linx/\$tumour_sample.linx.vis_sv_data.tsv \\
-exon \$run_dir/linx/\$tumour_sample.linx.vis_gene_exon.tsv \\
-cna \$run_dir/linx/\$tumour_sample.linx.vis_copy_number.tsv \\
-protein_domain \$run_dir/linx/\$tumour_sample.linx.vis_protein_domain.tsv \\
-fusion \$run_dir/linx/\$tumour_sample.linx.fusions_detailed.csv \\
-circos circos \\
-threads ${NUMCPUS}


#Loop over plots in linx output directory to add chromosome legends/ideotracks
for PNG in \$( ls $OUTPUTSAMPLEDIR/linx/plot/*.png )
do

echo \"\"
echo \"\"
echo \"Generating plot for file: \$PNG ..\"

BASE=\$( basename \$PNG .png )
PREFIX=\${BASE/.[0-9][0-9][0-9]/}


#Run rscript to fix chromosome annotation in plots
Rscript $GPLDIRECTORY/annotateLinxCircosPlotsWithChromosomalInformation.R \\
$OUTPUTSAMPLEDIR/linx/circos/\$PREFIX.chromosome.circos \\
$OUTPUTSAMPLEDIR/linx/circos/\$PREFIX.cytoBand.txt \\
\$PNG \\
45 \\
150 \\
6

done

cd -


#Create checksum for output
cd $OUTPUTSAMPLEDIR
#Add files to checksum here
cd -


#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

" >> $JOBFILE
    
done<$SAMPLEFILE




