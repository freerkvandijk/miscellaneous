# miscellaneous

This repository contains all kinds of miscellaneous scripts.


## Overview & Description


### checkDiskFree.sh
Checks how much diskspace is still available in /hpc/pmc_kuiper/ and sends an e-mail when a user specified percentage of free space is exceeded. Use in conjuction with crontab for example to check on regular basis.

### tree.sh
Displays a hierarchical directory structure of user specified input directory.

### annotateLinxCircosPlotsWithChromosomalInformation.R
R script which is modified from GRIDSS/PURPLE/LINX code to generate chromosomal annotations on the circos plots using hg38 as a reference sequence. Inputs are circos file, cytoband file, png image to annotate, fontsize, height, maxim columns

### convertVEPannotatedVCFtoTable.pl
Takes a VEP annotated VCF file as input and writes tabular delimited \*.txt file as output. Several output options can be selected via cmdline.

### copyAndArchiveAlignmentResults.sh
Copies all necessary results from the alignment pipeline to the PROCESSED drive and the project's RESULTS directory

### copyAndArchiveMantaResults.sh
Copies all necessary results from the Manta pipeline to the PROCESSED drive and the project's RESULTS directory

### copyGermlineCallingResults.sh
Copies all necessary results from the Single Sample Germline calling pipeline to the PROCESSED drive and the project's RESULTS directory

### createGnomadPLIbedFile.pl
Uses a Ensembl GTF file to annotate gnomAD 2.1 b37 data with b38 coordinates based on genenames and creates an output BED file containing regions and gnomad pLI scores

### createVCFfromGWAScatalog.pl
Takes a GWAS catalogue \*.tsv file and convert this to a VCF file, to be used with Ensembl VEP

### generateCNVanalysisJson.sh
Generates JSON files which are used as input for the Single Sample CNV calling pipeline. All default values are based on WGS samples.

### generateConvertBAMtoFastqJobs.sh
Generates jobs which can convert BAM files to Fastq files

### generateCreateUBAMfromFastqJobs.sh
Generates jobs which convert Fastq files to UBAMs, which are needed as input for the alignment/germline calling pipelines. This script uses a directory as input and extract information from filenames within that directory.

### generate_germline_single_sample_ParameterJSONs.sh 
Generates configuration JSON input parameter files for the Germline Single Sample calling pipeline. Input is a directory containing the sample merged UBAM files.

### generateGRIDSSjobs.sh
Generates jobs for GRIDSS/Purple/Linx Tumor-Normal sv calling, CNV calling and visualization.

### generateMantaNormalJobs.sh
Generate Manta jobs for Germline samples. The `SAMPLEFILE` parameter points to a file which per line contains tumor/normal sampleIDs to process. This file is tab-delimited, the first column contains the germline sampleIDs, the second column the Tumor sampleIDs. Each line contains one pair. An example file is depicted below.

```
germlineSampleID1	tumorSampleID1
germlineSampleID2	tumorSampleID2
germlineSampleID98	tumorSampleID98
```

### generateMantaTumorJobs.sh 
Generates Manta jobs for Tumor only samples. Input samplelist file is described above.

### generateMantaTumorNormalJobs.sh
Generates Manta jobs for Tumor/Normal sample pairs. Input samplelist file is described above.

### generateMergeUBAMsPerSampleJobs.sh
Generates jobs to merge UBAM files per sample, needed for alignment/germline calling pipeline. Input UBAM are generate by the `generateCreateUBAMfromFastqJobs.sh` script.

### generateMutect2CallingJobs.sh
Generates Mutect2 calling jobs per sample. Input is the tumor/normal samplelist file described above.

### generateMutect2FilterJobs.sh
Filters Mutect2 calling output and annotates all variants using VEP.

### generateUBAMfromBAM.sh
Generates jobs to convert BAM files to UBAMs.

### runAlignmentPipeline.sh
Starts the alignment pipeline.

### runGermlineSingleSamplePipeline.sh
Start the Single Sample Germline calling pipeline, using the input JSON files generated using the `generate_germline_single_sample_ParameterJSONs.sh` script.

