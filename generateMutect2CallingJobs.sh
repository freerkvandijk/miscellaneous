#Original code created by S. Lelieveld#

BATCHDIR="batch2"
PROJECTDIR="/<projectHomeDir>/"
JOBDIR="$PROJECTDIR/CODE/jobs/mutect2/$BATCHDIR/"
OUTPUTDIR="$PROJECTDIR/ANALYSIS/mutect2/$BATCHDIR/"
SAMPLEFILE="$PROJECTDIR/samplelists/sampleListMutect2.$BATCHDIR.Converted.txt"

mkdir -p "$JOBDIR"
mkdir -p "$OUTPUTDIR"

# set global variables
REF_GENOME="/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta"
NUM_CORES=4
NUM_THREADS=$(expr $NUM_CORES \* 2)

#FUNCTION: Join array with local IFS, to avoid preserving non-default IFS
join_arr() {
    local IFS="$1"
    shift
    echo "$*"
}

while read line
do
    
    #Read line in array
    declare -a Array
    Array=($line)
    
    #Set samples for job name usage
    SAMPLES=$(join_arr "." "${Array[@]}")
    
    #According to input file format the first sampleID is the normal, the others tumors
    NORMAL=${Array[0]}
    
    #Push all BAMs in array
    declare -a BAMs
    for SAMPLE in "${Array[@]}"
    do
        BAMs+=("-I $PROJECTDIR/RESULTS/GermlineCalling/$SAMPLE/call-ConvertToCram/$SAMPLE\_WGS.cram \\")
    done
    
    #Print all BAMs to be processed on a new line
    BAMstoProcess=$(printf "%s\n" "${BAMs[@]}")
    

#create job body
declare -a chromosomes=("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "X" "Y")

for CHR in "${chromosomes[@]}"
do
    
echo "
#$ -S /bin/bash
#$ -N mutect2.$SAMPLES.chr$CHR.sh
#$ -l h_vmem=16G
#$ -l tmpspace=300G
#$ -l h_rt=23:59:00
#$ -pe threaded $NUM_CORES 
#$ -o $JOBDIR/mutect2.$SAMPLES.chr$CHR.sh.out
#$ -e $JOBDIR/mutect2.$SAMPLES.chr$CHR.sh.err
#$ -cwd

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"


set -e


# load the required modules
module load gatk/4.1.1.0
module load Java/1.8.0_60

#run gatk
java -Djava.io.tmpdir=\$TMPDIR -Xmx12g -jar \$GATK4 \\
Mutect2 \\
-R $REF_GENOME \\
--native-pair-hmm-threads $NUM_THREADS \\
$BAMstoProcess
-normal $NORMAL \\
-L chr$CHR \\
--f1r2-tar-gz $OUTPUTDIR/$SAMPLES.chr$CHR-f1r2.tar.gz \\
-O $OUTPUTDIR/$SAMPLES.chr$CHR.vcf


#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

">$JOBDIR/mutect2.$SAMPLES.chr$CHR.sh

done

done< $SAMPLEFILE






