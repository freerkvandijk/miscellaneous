

####
BATCHDIR="batch2/"
PROJECTDIR="/hpc/pmc_kuiper/MRD4ALL/"
PARAMDIR="$PROJECTDIR/CODE/runConfigs/GenotypeCallingSingleSample/$BATCHDIR/"
#INPUTDIR="$PROJECTDIR/ANALYSIS/mergeUBAMsPerSample/$BATCHDIR/"
INPUTDIR="$PROJECTDIR/DATA/BAM/$BATCHDIR/"

####
EXPERIMENT_TYPE="Whole genome sequencing"
#EXPERIMENT_TYPE="Exome sequencing"
LIBRARY_STRATEGY="WGS"
#LIBRARY_STRATEGY="WXS"

if [[ $LIBRARY_STRATEGY == "WXS" ]]
then
    TARGET_INTERVALS_BED_FILE="/hpc/pmc_gen/references/hg38bundle/v0/MedExome_hg38_capture_targets.bed"
elif [[ $LIBRARY_STRATEGY == "WGS" ]]
then
    TARGET_INTERVALS_BED_FILE=""  
else
    echo "Type of library strategy not known: $LIBRARY_STRATEGY"
    exit
fi 
    
#
DATE=$(date --iso-8601=seconds)

mkdir -p "$PARAMDIR/"


for BAM in $( ls $INPUTDIR/*${LIBRARY_STRATEGY}.bam )
do
    
    SAMPLE=$( basename $BAM _${LIBRARY_STRATEGY}.bam )
    MD5SUM=$( head -n1 $BAM.md5 | awk '{print $1}' )
    
    
echo "
{
   \"GenotypeCallingSingleSample.biomaterial\" : \"$SAMPLE\",
   \"GenotypeCallingSingleSample.biomaterial_id\" : \"$SAMPLE\",
    \"GenotypeCallingSingleSample.url\" : \"$BAM\",
    \"GenotypeCallingSingleSample.library\" : \"$SAMPLE\",
    \"GenotypeCallingSingleSample.barcode\" : \"none\",
    \"GenotypeCallingSingleSample.md5\" : \"$MD5SUM\",
    \"GenotypeCallingSingleSample.run\" : \"$SAMPLE\",
   \"GenotypeCallingSingleSample.targetIntervalsBedFile\" : \"$TARGET_INTERVALS_BED_FILE\",
   \"GenotypeCallingSingleSample.cache_path\" : \"\",


   \"GenotypeCallingSingleSample.max_gaussians\" :\"2\",

   \"GenotypeCallingSingleSample.dbSNP_vcf_index\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dbsnp138.vcf.idx\",
   \"GenotypeCallingSingleSample.ref_bwt\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.bwt\",
   \"GenotypeCallingSingleSample.vep_assembly\" : \"GRCh38\",
   \"GenotypeCallingSingleSample.vep_cpu\" : \"4\",
   \"GenotypeCallingSingleSample.module_r_version\" : \"R/3.2.4\",
   \"GenotypeCallingSingleSample.known_indels_sites_indices\" : [
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.idx\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.known_indels.vcf.idx\"
   ],
   \"GenotypeCallingSingleSample.ref_fasta\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta\",
   \"GenotypeCallingSingleSample.fingerprint_genotypes_file\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/empty.fingerprint.vcf\",
   \"GenotypeCallingSingleSample.picard_baitsIntervalsBedFile\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/MedExome_hg38_capture_targets.interval_list\",
   \"GenotypeCallingSingleSample.vep_cache\" : \"/hpc/pmc_kuiper/References/vep92/cache\",
   \"GenotypeCallingSingleSample.molgenis_token\" : \"ae7e1bd567704b43b283a83d8c33aae0\",
   \"GenotypeCallingSingleSample.wallclock_ages\" : \"86:00:00\",
   \"GenotypeCallingSingleSample.molgenisMultiQCanalysis.refentity_id\" : null,
   \"GenotypeCallingSingleSample.vep_plugin_dir\" : \"/hpc/pmc_kuiper/References/vep92/plugin/\",
   \"GenotypeCallingSingleSample.gvcf_exp\" : \"*.g.vcf.gz\",
   \"GenotypeCallingSingleSample.module_multiqc_version\" : \"multiqc/1.4\",
   \"GenotypeCallingSingleSample.web_host\" : \"\",
   \"GenotypeCallingSingleSample.ref_sa\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.sa\",
   \"GenotypeCallingSingleSample.vep_annotations\" : [
      {
         \"fields\" : \"\",
         \"annotationtype\" : \"overlap\",
         \"file\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/genome_targets/hg38_diagnostic_germline_1.0.bed.gz\",
         \"index\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/genome_targets/hg38_diagnostic_germline_1.0.bed.gz.tbi\",
         \"name\" : \"Diag_Germline_Gene\",
         \"filetype\" : \"bed\",
        \"reportcoordinates\" : \"0\"
      }
   ],
   \"GenotypeCallingSingleSample.omni_vcf_index\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/1000G_omni2.5.hg38.vcf.gz.tbi\",
   \"GenotypeCallingSingleSample.wgs_coverage_interval_list\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/wgs_coverage_regions.hg38.interval_list\",
   \"GenotypeCallingSingleSample.module_autossh_version\" : \"autossh/1.4e\",
   \"GenotypeCallingSingleSample.picard_targetIntervalsBedFile\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/st_judes_somatic_canon_exons_sorted_1.2.interval_list\",
   \"GenotypeCallingSingleSample.vep_species\" : \"homo_sapiens\",
   \"GenotypeCallingSingleSample.ValidateAggregatedSamFile.max_output\" : null,
   \"GenotypeCallingSingleSample.workflow_id\" : \"germline_snv\",
   \"GenotypeCallingSingleSample.ValidateReadGroupSamFile.max_output\" : null,
   \"GenotypeCallingSingleSample.sub_strip_path\" : \"/.*/\",
   \"GenotypeCallingSingleSample.ConvertToCram.cram_exp\" : \"*.cra[im]\",
   \"GenotypeCallingSingleSample.module_gatk_version\" : \"gatk/all\",
   \"GenotypeCallingSingleSample.tmpspace_gb_moderate\" : \"40\",
   \"GenotypeCallingSingleSample.FastQCreadgroup.html_exp\" : \"*.html\",
   \"GenotypeCallingSingleSample.lowCoverageCutOff\" : \"10\",
   \"GenotypeCallingSingleSample.submission_date\" : \"$DATE\",
   \"GenotypeCallingSingleSample.ref_dict\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dict\",
   \"GenotypeCallingSingleSample.ValidateAggregatedSamFile.ignore\" : null,
   \"GenotypeCallingSingleSample.module_molgenistools_version\" : \"molgenistools/4\",
   \"GenotypeCallingSingleSample.molgenisCRAManalysis.refentity_id\" : null,
   \"GenotypeCallingSingleSample.data_host\" : \"hpct01\",
   \"GenotypeCallingSingleSample.variants_for_contamination_index\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/WholeGenomeShotgunContam.vcf.idx\",
   \"GenotypeCallingSingleSample.module_ensemblvep_version\" : \"ensemblvep/92\",
   \"GenotypeCallingSingleSample.tmpspace_gb_small\" : \"4\",
   \"GenotypeCallingSingleSample.strip_url\" : \"file://data/isi/p/pmc_research/omics\",
   \"GenotypeCallingSingleSample.hapmap_vcf\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/hapmap_3.3.hg38.vcf.gz\",
   \"GenotypeCallingSingleSample.module_gatk4_version\" : \"gatk/package-4.0.1.2-local\",
   \"GenotypeCallingSingleSample.vep_plugins\" : [
      {
         \"index\" : \"/hpc/pmc_kuiper/References/annotation/variants/grch38/ExAC.r0.3.1.sites.vep.vcf.gz.tbi\",
         \"file\" : \"/hpc/pmc_kuiper/References/annotation/variants/grch38/ExAC.r0.3.1.sites.vep.vcf.gz\",
         \"misc\" : null,
         \"fields\" : \"AC,AN\",
         \"filetype\" : \"vcf\",
         \"name\" : \"ExAC\"
      },
      {
         \"name\" : \"dbNSFP\",
         \"filetype\" : \"vcf\",
         \"misc\" : \"/hpc/pmc_kuiper/References/annotation/variants/grch38/dbNSFP3.5a.readme.txt\",
         \"fields\" : \"CADD_phred,gnomAD_exomes_AF,gnomAD_exomes_NFE_AF,ExAC_nonTCGA_AF,ExAC_nonTCGA_NFE_AF,gnomAD_genomes_AF,gnomAD_genomes_NFE_AF,phyloP100way_vertebrate,clinvar_rs,clinvar_clnsig,clinvar_trait,clinvar_golden_stars\",
         \"index\" : \"/hpc/pmc_kuiper/References/annotation/variants/grch38/dbNSFP3.5a.txt.gz.tbi\",
         \"file\" : \"/hpc/pmc_kuiper/References/annotation/variants/grch38/dbNSFP3.5a.txt.gz\"
      }
   ],
  \"GenotypeCallingSingleSample.file_id_exp\" : \"[a-z0-9]{32}\",
   \"GenotypeCallingSingleSample.module_python3_version\" : \"python/3.4.3\",
   \"GenotypeCallingSingleSample.haplotype_database_file\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/empty.haplotype_map.txt\",
   \"GenotypeCallingSingleSample.wallclock_short\" : \"4:00:00\",
   \"GenotypeCallingSingleSample.ValidateReadGroupSamFile.ignore\" : null,
   \"GenotypeCallingSingleSample.ref_ann\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.ann\",
   \"GenotypeCallingSingleSample.ref_fasta_index\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.fai\",
   \"GenotypeCallingSingleSample.wgs_calling_interval_list\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/wgs_calling_regions.hg38.interval_list\",
   \"GenotypeCallingSingleSample.wallclock_long\" : \"36:00:00\",
   \"GenotypeCallingSingleSample.wgs_evaluation_interval_list\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/wgs_evaluation_regions.hg38.interval_list\",
   \"GenotypeCallingSingleSample.molgenisNonRefVARIANTanalysis.refentity_id\" : null,
   \"GenotypeCallingSingleSample.molgenisVARIANTanalysis.refentity_id\" : null,
   \"GenotypeCallingSingleSample.variants_for_contamination\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/qc/WholeGenomeShotgunContam.vcf\",
   \"GenotypeCallingSingleSample.study\" : \"$SAMPLE\",
   \"GenotypeCallingSingleSample.multiqc_config_file\" : \"/hpc/pmc_kuiper/configurations/multiqc/germline_snv.yaml\",
   \"GenotypeCallingSingleSample.module_fastqc_version\" : \"fastqc/0.11.5\",
   \"GenotypeCallingSingleSample.tmpspace_gb_huge\" : \"800\",
   \"GenotypeCallingSingleSample.scattered_calling_intervals\" : [
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0001_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0002_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0003_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0004_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0005_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0006_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0007_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0008_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0009_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0010_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0011_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0012_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0013_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0014_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0015_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0016_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0017_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0018_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0019_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0020_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0021_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0022_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0023_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0024_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0025_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0026_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0027_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0028_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0029_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0030_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0031_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0032_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0033_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0034_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0035_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0036_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0037_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0038_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0039_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0040_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0041_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0042_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0043_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0044_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0045_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0046_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0047_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0048_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0049_of_50/scattered.interval_list\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/scattered_calling_intervals/temp_0050_of_50/scattered.interval_list\"
   ],
   \"GenotypeCallingSingleSample.module_python_version\" : \"python/2.7.10\",
   \"GenotypeCallingSingleSample.module_samtools_version\" : \"samtools/1.3\",
   \"GenotypeCallingSingleSample.dbSNP_vcf\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dbsnp138.vcf\",
   \"GenotypeCallingSingleSample.module_verifyBamID_version\" : \"verifyBamID/1.1.13\",
   \"GenotypeCallingSingleSample.molgenisCRAManalysis.refentity\" : null,
   \"GenotypeCallingSingleSample.baitsIntervalsBedFile\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/MedExome_hg38_capture_targets.bed\",
  \"GenotypeCallingSingleSample.ref_amb\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.amb\",
   \"GenotypeCallingSingleSample.omni_vcf\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/1000G_omni2.5.hg38.vcf.gz\",
   \"GenotypeCallingSingleSample.derived_data_path\" : \"/data/isi/p/pmc_research/omics\",
   \"GenotypeCallingSingleSample.hapmap_vcf_index\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/hapmap_3.3.hg38.vcf.gz.tbi\",
   \"GenotypeCallingSingleSample.molgenisMultiQCanalysis.refentity\" : null,
   \"GenotypeCallingSingleSample.library_strategy\" : \"$LIBRARY_STRATEGY\",
   \"GenotypeCallingSingleSample.target_host\" : \"hpct01\",
   \"GenotypeCallingSingleSample.wallclock_moderate\" : \"8:00:00\",
   \"GenotypeCallingSingleSample.module_picard_version\" : \"picardtools/2.10.10\",
   \"GenotypeCallingSingleSample.unmapped_bam_suffix\" : \".ubam\",
   \"GenotypeCallingSingleSample.molgenisVARIANTanalysis.refentity\" : null,
   \"GenotypeCallingSingleSample.module_bwa_version\" : \"bwa/0.7.13\",
   \"GenotypeCallingSingleSample.molgenis_host\" : \"\",
   \"GenotypeCallingSingleSample.vep_output_format\" : \"vcf\",
   \"GenotypeCallingSingleSample.experiment_ids\" : [
      \"$SAMPLE\"
   ],
   \"GenotypeCallingSingleSample.run_ids\" : [
      \"$SAMPLE\"
   ],
   \"GenotypeCallingSingleSample.FastQCreadgroup.zip_exp\" : \"*.zip\",
   \"GenotypeCallingSingleSample.vcf_exp\" : \"*.vcf.gz\",
   \"GenotypeCallingSingleSample.reheader_bam\" : \"true\",
   \"GenotypeCallingSingleSample.module_bedtools_version\" : \"bedtools/2.25.0\",
   \"GenotypeCallingSingleSample.known_indels_sites_VCFs\" : [
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf\",
      \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.known_indels.vcf\"
   ],
  \"GenotypeCallingSingleSample.multiqc_command_line_config\" : \"{ report_comment: 'This report was generated for the <code>Germline SNV</code> pipeline' }\",
   \"GenotypeCallingSingleSample.vep_fields\" : [
      \"Location\",
      \"Allele\",
      \"VARIANT_CLASS\",
      \"Gene\",
      \"Feature\",
      \"SYMBOL\",
      \"CCDS\",\"STRAND\",
      \"IMPACT\",
      \"Consequence\",
      \"SIFT\",
      \"PolyPhen\",
      \"CADD_phred\",
      \"EXON\",
      \"DISTANCE\",
      \"COSMIC_ID\",
      \"COSMIC_CNT\"
   ],
   \"GenotypeCallingSingleSample.ref_alt\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.alt\",
   \"GenotypeCallingSingleSample.molgenisNonRefVARIANTanalysis.refentity\" : null,
   \"GenotypeCallingSingleSample.experiment_type\" : \"$EXPERIMENT_TYPE\",
   \"GenotypeCallingSingleSample.ref_pac\" : \"/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta.64.pac\"
}

">$PARAMDIR/sample.$SAMPLE.json
    
    
    
    
    
    
done
