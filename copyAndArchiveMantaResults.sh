
BATCHDIR="batch2"

PROJECTDIR="/<projectHomeDir>/"
ANALYSISDIR="$PROJECTDIR/ANALYSIS/manta/$BATCHDIR/"
OUTPUTDIR="$PROJECTDIR/RESULTS/manta/sampleResults/"
PROCESSEDDIR="/<projectHomeDirOnProcessedDrive>/"
PROCESSEDRESULTSDIR="$PROCESSEDDIR/RESULTS/manta/"
GENEFUSIONDIR="$PROCESSEDRESULTSDIR/geneFusions/"
SAMPLERESULTSDIR="$PROCESSEDRESULTSDIR/sampleResults/"

BATCHFILE="$PROJECTDIR/samplelists/toProcess.$BATCHDIR.txt"

DATE=$(date --iso-8601=seconds)


mkdir -p "$GENEFUSIONDIR"
mkdir -p "SAMPLERESULTSDIR"

while read line
do

    SAMPLE=$( basename $line | awk '{print $4}' FS="_" )
    echo "$SAMPLE"
    SAMPLEDIR="$OUTPUTDIR/$SAMPLE/"
    mkdir -p $SAMPLEDIR
    
    #Copy all results per sample to RESULTS dir
    mkdir -p $OUTPUTDIR/$SAMPLE/
    cp -Lavr $ANALYSISDIR/$SAMPLE/results/* $OUTPUTDIR/$SAMPLE/
    
    #Copy all results per sample to PROCESSED dir
    mkdir -p $SAMPLERESULTSDIR/$SAMPLE/
    cp -Lavr $ANALYSISDIR/$SAMPLE/results/* $SAMPLERESULTSDIR/$SAMPLE/
    
    #Copy only geneFusions to PROCESSED dir
    cp -Lav $ANALYSISDIR/$SAMPLE/results/variants/$SAMPLE.manta.output.annotated.tx* $GENEFUSIONDIR
   
done<$BATCHFILE
