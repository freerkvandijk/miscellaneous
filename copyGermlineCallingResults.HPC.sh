

########################################################
##### USER PARAMETERS, change to what you need
### Hashes to copy
#declare -A my_array
my_array=( 61bcfae1-e6bc-4ef1-a152-e10b29f967eb 2527617f-a7da-461d-b633-c1393aa100c6 \
4381c3b3-3269-4f62-9b07-df97b6f05163 \
)

### Project specific parameters
LIBRARY_STRATEGY="WGS"
PROJECTNAME="DNARepairAndCongenitalImmunodeficiencySyndromes"
CROMWELLDIR="/hpc/pmc_kuiper/fvandijk2/cromwell-executions/GermlineSingleSample/"
OUTPUTDIR="/hpc/pmc_kuiper/$PROJECTNAME/RESULTS/GermlineCalling/"


### Copy BAM files to results dir on HPC
# Switch, if set to YES it will copy the BAM file to the RESULTS directory on HPC.
COPYBAM="YES"
#
###
#####
########################################################



#Function to check if cachecopy directory is present
function checkCacheCopy()
{
    local dir=$1
    local ext=$2

    #Retrieve perl ( it somehow doesn't work from bash )
    perl=$( which perl )
    FIND=$(find ${dir} -name "*${ext}" | head -n1) #Find directory
    DIR=$( dirname $FIND )
    #NEWPATH=$( echo "$DIR" | sed "s/shard-[0-9]\+/shard*/g")
    #echo ${NEWPATH}
    echo $DIR
}


for HASH in "${my_array[@]}"
do
    echo -e -n "\n### Processing data for ...\n"
    echo -e -n "\n### HASH: $HASH\n"
    INPUTDIR="$CROMWELLDIR/$HASH/"
    
    #Do recursive find for cram file to check whether there is an additional cacheCopy directory in the path
    FIND=$(find $CROMWELLDIR/$HASH/call-ConvertToCram/ -name "*${LIBRARY_STRATEGY}.cram")
    
    if [[ $FIND == *"cacheCopy"* ]] #If FIND result contains cacheCopy do ...
    then
        CACHECOPY="YES"
    else
        CACHECOPY="NO"
    fi


    ###Extract sample name
    ##
    if [[ "$CACHECOPY" == "NO" ]]
    then
        INPUTDIRBASE="$INPUTDIR/call-ConvertToCram/"
    else
        INPUTDIRBASE="$INPUTDIR/call-ConvertToCram/cacheCopy/"
    fi
        
    SAMPLE=$( basename $INPUTDIRBASE/execution/*${LIBRARY_STRATEGY}.cram _${LIBRARY_STRATEGY}.cram )
    echo -e -n "### SAMPLE: $SAMPLE\n"
    SAMPLEDIR="$OUTPUTDIR/$SAMPLE/"
    mkdir -p $SAMPLEDIR
    
    
    ###QualityYieldMetrics
    ##
    echo -e -n "\n\n\n### Copying: QualityYieldMetrics\n\n"
    DIR="$INPUTDIR/call-CollectQualityYieldMetrics/"
    EXT=".unmapped.quality_yield_metrics"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectQualityYieldMetrics/
    rsync --progress -avr $(readlink -e $BASE/*.unmapped.quality_yield_metrics) $SAMPLEDIR/call-CollectQualityYieldMetrics/
    
    
    ###UnsortedReadgroupBamQualityMetrics
    ##
    echo -e -n "\n\n\n### Copying: UnsortedReadgroupBamQualityMetrics\n\n"
    DIR="$INPUTDIR/call-CollectUnsortedReadgroupBamQualityMetrics/"
    EXT=".pdf"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    rsync --progress -avr $(readlink -e $BASE/*.pdf) $SAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    rsync --progress -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectUnsortedReadgroupBamQualityMetrics/
    
    
    ###MarkDuplicates
    ##
    echo -e -n "\n\n\n### Copying: MarkDuplicates\n\n"
    DIR="$INPUTDIR/call-MarkDuplicates/"
    EXT="metrics"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-MarkDuplicates/
    rsync --progress -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-MarkDuplicates/
    
    
    #Check if CRAM file md5sum file exists and is not empty
    echo -e -n "\n\n\n### Checking if CRAM file md5sum exists\n\n"
    DIR="$INPUTDIR/call-ConvertToCram/"
    EXT="${LIBRARY_STRATEGY}.cram"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    if [ -s $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.cram.md5 ]
    then
        echo "MD5SUM file exists: $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.cram.md5"
    else
        echo "MD5SUM file does not exist: $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.cram.md5"
        echo "Generating it now ..";
        cd $BASE
        md5sum $SAMPLE\_${LIBRARY_STRATEGY}.cram > $SAMPLE\_${LIBRARY_STRATEGY}.cram.md5
        cd -
    fi
        
    
    ###AggregationMetrics
    ##
    echo -e -n "\n\n\n### Copying: AggregationMetrics\n\n"
    DIR="$INPUTDIR/call-CollectAggregationMetrics/"
    EXT=".pdf"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectAggregationMetrics/
    rsync --progress -avr $(readlink -e $BASE/*.pdf) $SAMPLEDIR/call-CollectAggregationMetrics/
    rsync --progress -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectAggregationMetrics/
    
    
    ###WgsMetrics
    ##
    echo -e -n "\n\n\n### Copying: WgsMetrics\n\n"
    DIR="$INPUTDIR/call-CollectWgsMetrics/"
    EXT="metrics"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectWgsMetrics/
    rsync --progress -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectWgsMetrics/
    
    
    ###ReadgroupBamQualityMetrics
    ##
    echo -e -n "\n\n\n### Copying: ReadgroupBamQualityMetrics\n\n"
    DIR="$INPUTDIR/call-CollectReadgroupBamQualityMetrics/"
    EXT=".pdf"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    rsync --progress -avr $(readlink -e $BASE/*.pdf) $SAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    rsync --progress -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectReadgroupBamQualityMetrics/
    
    ###RawWgsMetrics
    ##
    echo -e -n "\n\n\n### Copying: RawWgsMetrics\n\n"
    DIR="$INPUTDIR/call-CollectRawWgsMetrics/"
    EXT="metrics"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectRawWgsMetrics/
    rsync --progress -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectRawWgsMetrics/
    
    
    ###GvcfCallingMetrics
    ##
    echo -e -n "\n\n\n### Copying: GvcfCallingMetrics\n\n"
    DIR="$INPUTDIR/call-CollectGvcfCallingMetrics/"
    EXT="metrics"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectGvcfCallingMetrics/
    rsync --progress -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectGvcfCallingMetrics/
    
    ###CallingMetrics
    ##
    echo -e -n "\n\n\n### Copying: CallingMetrics\n\n"
    DIR="$INPUTDIR/call-CollectCallingMetrics/"
    EXT="metrics"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-CollectCallingMetrics/
    rsync --progress -avr $(readlink -e $BASE/*metrics) $SAMPLEDIR/call-CollectCallingMetrics/
    
    
    ###GatherQCreports
    ##
    echo -e -n "\n\n\n### Copying: GatherQCreports\n\n"
    DIR="$INPUTDIR/call-gatherQCreports/"
    EXT="data.zip"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-gatherQCreports/
    rsync --progress -avr $(readlink -e $BASE/*multiqc*) $SAMPLEDIR/call-gatherQCreports/
    
    ###VepAnnotationOutput
    ##
#!    ########## merged VEP annotated VCF file in different folder structure in cromwell/WDL pipeline v6
#Example: /hpc/pmc_kuiper/fvandijk2/cromwell-executions/GermlineSingleSample/27ae3284-d054-4de3-af05-84f9019cf8eb/call-ensemblVep/Vep.ensemblVep/3beed5ed-b90e-4001-b88d-80bfd5e981bb/call-MergeVCFs/execution/PMRBM000ALQ_WXS.vcf.gz
    echo -e -n "\n\n\n### Copying: VepAnnotationOutput\n\n"     
    DIR="$INPUTDIR/call-ensemblVep/Vep.ensemblVep/*/call*/"
    EXT="${LIBRARY_STRATEGY}.vcf.gz"
    BASE=$(checkCacheCopy "$DIR" "$EXT")

    if [ -s $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.vcf.gz.md5 ]
    then
        echo "MD5SUM file exists: $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.vcf.gz.md5"
    else
        echo "MD5SUM file does not exist: $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.vcf.gz.md5"
        echo "Generating it now ..";
        cd $BASE
        md5sum $SAMPLE\_${LIBRARY_STRATEGY}.vcf.gz > $SAMPLE\_${LIBRARY_STRATEGY}.vcf.gz.md5
        cd -
    fi

    mkdir -p $SAMPLEDIR/call-ensemblVep/
    rsync --progress -avr $(readlink -e $BASE/*vcf*) $SAMPLEDIR/call-ensemblVep/
    
    
    ###MergeGVCFS
    ##
    echo -e -n "\n\n\n### Copying: MergeGVCFS\n\n"
    DIR="$INPUTDIR/call-MergeGVCFS/"
    EXT="${LIBRARY_STRATEGY}.g.vcf.gz"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    if [ -s $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.g.vcf.gz.md5 ]
    then
        echo "MD5SUM file exists: $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.g.vcf.gz.md5"
    else
        echo "MD5SUM file does not exist: $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.g.vcf.gz.md5"
        echo "Generating it now ..";
        cd $BASE
        md5sum $SAMPLE\_${LIBRARY_STRATEGY}.g.vcf.gz > $SAMPLE\_${LIBRARY_STRATEGY}.g.vcf.gz.md5
        cd -
    fi
    
    mkdir -p $SAMPLEDIR/call-MergeGVCFS/
    rsync --progress -avr $(readlink -e $BASE/*vcf*) $SAMPLEDIR/call-MergeGVCFS/

    
    #ConvertToCram to HPC result dir
    echo -e -n "\n\n\n### Copying: ConvertToCram\n\n"  
    DIR="$INPUTDIR/call-ConvertToCram/"
    EXT="${LIBRARY_STRATEGY}.cram"
    BASE=$(checkCacheCopy "$DIR" "$EXT")
    
    mkdir -p $SAMPLEDIR/call-ConvertToCram/
    rsync --progress -avr $(readlink -e $BASE/*.cra*) $SAMPLEDIR/call-ConvertToCram/
    
    #GatherBamFiles
    if [ $COPYBAM == "YES" ]
    then
        
        echo -e -n "\n\n\n### Copying: GatherBamFiles\n\n"  
        DIR="$INPUTDIR/call-GatherBamFiles/"
        EXT="${LIBRARY_STRATEGY}.bam"
        BASE=$(checkCacheCopy "$DIR" "$EXT")
        
        #Check if BAM file md5sum file exists and is not empty
        if [ -s $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.bam.md5 ]
        then
            echo "MD5SUM file exists: $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.bam.md5"
        else
            echo "MD5SUM file does not exist: $BASE/$SAMPLE\_${LIBRARY_STRATEGY}.bam.md5"
            echo "Generating it now ..";
            cd $BASE
            md5sum $SAMPLE\_${LIBRARY_STRATEGY}.bam > $SAMPLE\_${LIBRARY_STRATEGY}.bam.md5
            cd -
        fi
        
        #Copy BAM file to RESULTS on HPC, use as input for SNV-analysis etc.
        mkdir -p $SAMPLEDIR/call-GatherBamFiles/
        rsync --progress -avr $(readlink -e $BASE/*.ba*) $SAMPLEDIR/call-GatherBamFiles/
    
    fi

    echo -e -n "\n### Finished copying all data for hash: $HASH\n\n\n"  
    
    
done

echo "Finished copying all data!"









