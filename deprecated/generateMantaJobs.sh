



BATCHDIR="batch6"
PROJECTDIR="/<projectHomeDir>/"
INPUTDIR="$PROJECTDIR/DATA/WGS/BAM/$BATCHDIR/"
JOBDIR="$PROJECTDIR/CODE/jobs/manta/$BATCHDIR/"
OUTPUTDIR="$PROJECTDIR/ANALYSIS/manta/$BATCHDIR/"

BATCHFILE="$PROJECTDIR/samplelists/toProcess.$BATCHDIR.txt"

DATE=$(date --iso-8601=seconds)

mkdir -p $JOBDIR
mkdir -p $OUTPUTDIR


while read line
do

    SAMPLE=$( basename $line | awk '{print $4}' FS="_" )
    BAM="$PROJECTDIR/RESULTS/AlignmentSingleSample/$SAMPLE/call-GatherBamFiles/$SAMPLE\_WGS.bam"

echo "
#$ -S /bin/bash
#$ -N sample_$SAMPLE.manta.sh
#$ -l h_vmem=60G
#$ -l tmpspace=300G
#$ -l h_rt=05:59:00
#$ -pe threaded 26
#$ -o $JOBDIR/$SAMPLE.manta.sh.out
#$ -e $JOBDIR/$SAMPLE.manta.sh.err
#$ -cwd

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"

#Load module
module load manta/1.6.0
module load python/2.7.10
module load samtools/1.3
module load R/3.2.2

#Make output dir
mkdir -p $OUTPUTDIR/$SAMPLE/

cd $OUTPUTDIR/$SAMPLE/

#..manta configuration..
python /hpc/local/CentOS7/gen/software/manta/1.6.0/bin/configManta.py \\
--tumorBam=$BAM \\
--referenceFasta=/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta \\
--callRegions=/hpc/pmc_kuiper/MRD4ALL/DATA/SV_analysis/GRCh38.bed.gz \\
--runDir=$OUTPUTDIR/$SAMPLE/

#..run manta..
python $OUTPUTDIR/$SAMPLE/runWorkflow.py -j 25

#..reformat inversions into single inverted sequence junctions..
python /hpc/local/CentOS7/gen/software/manta/1.6.0/libexec/convertInversion.py \\
/hpc/local/CentOS7/gen/software/samtools-1.3/bin/samtools \\
/hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.fasta \\
$OUTPUTDIR/$SAMPLE/results/variants/tumorSV.vcf.gz \\
> $OUTPUTDIR/$SAMPLE/results/variants/tumorSV_v2.vcf

# Run rscript to annotate results
Rscript /hpc/pmc_kuiper/MRD4ALL/CODE/Manta_parser_cmdline.Rscript \\
$OUTPUTDIR/$SAMPLE/results/variants/tumorSV_v2.vcf \\
$OUTPUTDIR/$SAMPLE/results/variants/$SAMPLE.manta.output.annotated.txt

cd -

#Generate md5sums
cd $OUTPUTDIR/$SAMPLE/results/variants/

md5sum tumorSV.vcf.gz > tumorSV.vcf.gz.md5
md5sum candidateSV.vcf.gz > candidateSV.vcf.gz.md5
md5sum candidateSmallIndels.vcf.gz > candidateSmallIndels.vcf.gz.md5
gzip tumorSV_v2.vcf
md5sum tumorSV_v2.vcf.gz > tumorSV_v2.vcf.gz.md5
md5sum $SAMPLE.manta.output.annotated.txt > $SAMPLE.manta.output.annotated.txt.md5

cd -

#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

" > $JOBDIR/$SAMPLE.manta.sh

done<$BATCHFILE

