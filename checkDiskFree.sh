#!/usr/bin/env bash

#Set limits in percentages
HARDLIMIT="95"

#Set group directory
GROUPDIR="/hpc/pmc_kuiper/"

#Retrieve used storage space
USED=$(df $GROUPDIR | tail -n1 | awk '{print $3}')

#Retrieve used storage space in human readable format
USEDHR=$(df -h $GROUPDIR | tail -n1 | awk '{print $3}')

#Retrieve maximum allowed storage space
MAX=$(df $GROUPDIR | tail -n1 | awk '{print $2}')

#Retrieve maximum allowed storage space in human readable format
MAXHR=$(df -h $GROUPDIR | tail -n1 | awk '{print $2}')

#Calculate percentage used
PERC=$( echo "$USED/$MAX*100" | bc -l )

#Check if max limit exceeded
if [ -n "$PERC" -a -n "$HARDLIMIT" ] #Comparison value and threshold, send e-mail when over limit
	then

	#Result of PERC > HARDLIMIT
	result=$(awk -vn1="$PERC" -vn2="$HARDLIMIT" 'BEGIN{print (n1>n2)?1:0 }')
	
	if [ "$result" -eq 1 ]
	then

		#Create body of e-mail
		BODY=$( echo -e -n "We have used more than $HARDLIMIT% of our storage quota. Please start cleaning up unnecessary data as soon as possible!\nUsed: $USEDHR\nQuota: $MAXHR\n")

		#Echo body and send mail (needs echo command, because mail expects input from keyboard)
		echo "$BODY" | mail -s "QUOTA ALERT: more than $HARDLIMIT% of quota used" "L.Morgado@prinsesmaximacentrum.nl,S.H.Lelieveld@prinsesmaximacentrum.nl,f.vandijk-5@prinsesmaximacentrum.nl,freerk.van.dijk@gmail.com"
    fi
fi

