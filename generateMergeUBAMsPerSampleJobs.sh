

BATCHDIR="batch2/"
PROJECTDIR="/hpc/pmc_kuiper/<PROJECTNAME>/"
DATADIR="$PROJECTDIR/DATA/fastq/"
JOBDIR="$PROJECTDIR/CODE/jobs/mergeUBAMsPerSample/$BATCHDIR/"
INPUTDIR="$PROJECTDIR/ANALYSIS/createUBAMfromFastq/$BATCHDIR/"
OUTPUTDIR="$PROJECTDIR/ANALYSIS/mergeUBAMsPerSample/$BATCHDIR/"

DATE=$(date --iso-8601=seconds)

#Make output directories
mkdir -p $JOBDIR
mkdir -p $OUTPUTDIR

#Changedir to input directory
cd $INPUTDIR

#Declare array
ARRAY=()

#Loop over all ubams in directory, ASSUMES SAMPLENAME IS THE FIRST PART OF THE UBAM FILENAME!!!
for BAM in $( ls *.ubam )
do
    SAMPLE=$(echo $BAM | awk '{print $1}' FS="_") #Extract samplename
    ARRAY+=($SAMPLE) #Push samplename in hash
done

#Extract only unique IDs from array
sorted_unique_ids=($(echo "${ARRAY[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

#Change back to previous directory
cd -

#Loop over all samples in array and generate jobs
for SAMPLE in "${sorted_unique_ids[@]}"
do
    echo "Generating job for sample: $SAMPLE"
    
    
    echo "
#$ -S /bin/bash
#$ -N sample.$SAMPLE.mergeUBAMsPerSample.sh
#$ -l h_vmem=12G
#$ -l tmpspace=200G
#$ -l h_rt=09:59:00
#$ -o $JOBDIR/$SAMPLE.mergeUBAMsPerSample.sh.out
#$ -e $JOBDIR/$SAMPLE.mergeUBAMsPerSample.sh.err
#$ -cwd

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"

#Load module
module load picardtools/2.10.10

#Run command
java -Xmx8g -Djava.io.tmpdir=\$TMPDIR -jar \$PICARD MergeSamFiles \\" > $JOBDIR/$SAMPLE.mergeUBAMsPerSample.sh

for BAM in $( ls $INPUTDIR/$SAMPLE*.ubam)
do
    echo "INPUT=$BAM \\" >> $JOBDIR/$SAMPLE.mergeUBAMsPerSample.sh
done

echo "OUTPUT=$OUTPUTDIR/$SAMPLE.ubam



#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

" >> $JOBDIR/$SAMPLE.mergeUBAMsPerSample.sh
    
    
    
    
    
done

