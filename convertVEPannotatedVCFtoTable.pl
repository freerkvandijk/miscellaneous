#!/usr/bin/perl -w
use strict;
use warnings;
use diagnostics;
use List::Util qw(min max);
use List::Util qw(sum);
use List::Util qw(first);
use List::MoreUtils qw/ uniq /;
use File::Glob ':glob';
use File::Basename;
use Getopt::Long;

#Version
my $version = "v1.0";

#Commandline variables
my ($help, $vcf, $tab, $type, $sample, @popMax);

#### get options
GetOptions(
                "h"                     => \$help,
                "vcf=s"                 => \$vcf,
                "tab=s"                 => \$tab,
                "type=s"                => \$type,
                "popmax:s"              => \@popMax, #Optional argument, comma seperated list of annotations to use for pop max calculation
                "sample:s"              => \$sample #Optional argument, only used when type is singlesample
          );
usage() and exit(1) if $help;
#Obligatory args
usage() and exit(1) unless $vcf;
usage() and exit(1) unless $tab;
usage() and exit(1) unless $type;

if ($type eq "singlesample" || $type eq "multisample") {
    #continue
    if ($type eq "singlesample" && length($sample) == 0){
        die "Type singlesample is used, but not sample name is specified. Please read the manual.\n";
    }
    if ($type eq "multisample" && defined $sample ){
        die "It is not possible to use type multisample in combination with the sample parameter. Please read the manual.\n";
    }
}else{ #Throw error
    die "VCF type $type is not supported. Please read the manual.\n";
}

#Dereference popmax array into array with element
@popMax = split(/,/,join(',',@popMax));

#Retrieve and print starttime
my $starttime = localtime();
print "\nStarting conversion: $starttime\n";

#Input VCF file
#my $file = "/hpc/pmc_kuiper/fvandijk2/projects/bladderCancer/variantAnnotation/MF001011_pass.hg38_multianno.rareVariants.leftAligned.vepAnnotated.vcf";
#my $file = "/hpc/pmc_kuiper/fvandijk2/projects/bladderCancer/variantAnnotation/test.vcf";

#Output tabular txt file
#my $outputFile = "/hpc/pmc_kuiper/fvandijk2/projects/bladderCancer/variantAnnotation/MF001011_pass.hg38_multianno.rareVariants.leftAligned.vepAnnotated.tab.txt";

#Read VCF file
my $headerCMD;
if ($vcf =~ /.gz$/) { #Check if zipped, if true decompress file on the fly
    open(INPUT, "gunzip -c $vcf |") || die "can’t open pipe to $vcf\n";
    $headerCMD = "zcat $vcf | head -n10000 | grep '^#'";
}else {
    open(INPUT, "< $vcf") || die "can’t open $vcf\n";
    $headerCMD = "head -n10000 $vcf | grep '^#'";
}

#Execute header command
my $header = `$headerCMD`;
my @headerArray = split(/\n/, $header); #Push header in array by line

#Process header and create output tabular header line
my @infoArray;
my @formatArray;
my @sampleArray;
my @csqArray;
my %csqHash;
push(@formatArray, "GT"); #GT is always the first element in format column
my @headerLineArray;
foreach my $line (@headerArray){
    chomp($line);
    ##FORMAT=<ID=AD,Number=.,Type=Integer,Description="Allelic depths for the ref and alt alleles in the order listed">
    my $type;
    my $prop;
    my $description;
    if ($line =~ m/^##(.+)=<ID=(.+),Numb.+Description="(.+)">/gs){ #Extract type, properties and description from header lines
        $type = $1;
        $prop = $2;
        $description = $3;
        if ( $prop ne "GT" && $type eq "FORMAT"){ #Push AD and new seperate columns holding allele counts in array
            if ( $prop eq "AD" ){
                push(@formatArray, "AD");
                push(@formatArray, "REFCOUNT");
                push(@formatArray, "ALTCOUNT");
                push(@formatArray, "VAF");
            }else{
                push(@formatArray, "$prop");
            }
        }
        if ($type eq "INFO"){ #If info column push all values in array
            if (not exists $csqHash{ $prop }){
                push(@infoArray, "$prop");
            }
            if ( $prop eq "CSQ" ){ #If CSQ, annotations from VEP
                $description =~ s/Consequence annotations from Ensembl VEP. Format: //gs;
                @csqArray = split(/\|/, $description);
                foreach my $ann (@csqArray){
                    $csqHash{ $ann } = $ann;
                }
            }
        }
    }elsif($line =~ m/^#CHRO.+/gs){ #Extract header line
        $line = substr($line, 1); #Remove starting # from line
        @headerLineArray = split("\t", $line);
    }else{
        #There might be some other non-compliant VCF lines in the header
    }
}
#Add popMax column if specified on input cmdline
if (@popMax != 0){ #If popMax defined add to header
    $csqHash{ "popMax" } = "popMax";
    push(@csqArray, "popMax");
}

#New output header
my @outputHeader = @headerLineArray[0..8]; #Start creating new output header
push(@outputHeader, @csqArray);
push(@outputHeader, @infoArray);
for (my $n=9; $n<=$#headerLineArray; $n++){ #Loop over samples in VCF file
    my $sampleName = $headerLineArray[$n];
    if ($type eq "singlesample" && defined $sample && $sample ne ''){ #If sample switch is used
        $n=100;
        $sampleName = $sample;
    }
    push(@outputHeader, "$sampleName"); #Push sample names in array
    foreach my $val (@formatArray){ #For the specific format column create headers in the format SAMPLENAME.PROPERTY
        push(@outputHeader, "$sampleName.$val");
    }
}
my $outputHeader = join("\t", @outputHeader); #Join array by tab, creating a string to print in output file
open(OUTPUT, "> $tab") || die "can’t open $tab\n";
print OUTPUT "$outputHeader\n";


#Process variant lines
while (my $line = <INPUT>){
    chomp($line);
    if ($line !~ m/^#.+/gs){ #variantline
        my %varProps;
        my @array = split("\t", $line); #Extract column data
        my $chr = $array[0];
        my $pos = $array[1];
        my $rsid = $array[2];
        my $ref = $array[3];
        my $alt = $array[4];
        my $qual = $array[5];
        my $filter = $array[6];
        my $info  = $array[7];
        for (my $l=0; $l<=8; $l++){ #First 8 values, push in array
            my $prop = $headerLineArray[$l];
            my $val = $array[$l];
            $varProps{ $prop } = $val;
        }
        my @ar = split(";", $info); #Iterate over info column, extract all properties and values
        foreach my $ele (@ar){
            my @array = split("=", $ele);
            my $prop = $array[0];
            my $val = $array[1];
            if ($prop eq "CSQ"){ #If CSQ field iterate over it
                my @csq = split(/\|/, $val);
                for (my $i=0; $i<=$#csqArray; $i++){
                    my $csqProp = $csqArray[$i];
                    my $csqVal = $csq[$i];
                    if (defined $csqVal && $csqVal ne ''){
                        #bla
                    }else{
                        $csqVal=".";
                    }
                    $varProps{ $csqProp } = $csqVal;
                }
            }else{
                if (defined $val && $val ne ''){
                    #bla
                }else{
                    $val=".";
                }
                $varProps{ $prop } = $val;
            }
        }
        #Iterate over popMax array and extract key value pairs to sort if defined on cmdline
        if (@popMax != 0){
            my @AFstoSort;
            foreach my $popMaxProp (@popMax){
                #Check if exists, otherwise die with error
                if (exists $varProps{ $popMaxProp }){
                    my $AF = $varProps{ $popMaxProp };
                    if ($AF !~ m/^\.$/gs){ #if not matches a dot, add to array
                        push(@AFstoSort, $AF);
                    }
                }else{
                    die "Popmax property named $popMaxProp cannot be found in the VCF header.\n";
                }
            }
            #Check if array does contain values, otherwide don't sort
            my $popMax = ".";
            if (@AFstoSort != 0){ #if array is not empty
                my @AFsSorted = sort { $b <=> $a } @AFstoSort; #sort, highest first
                $popMax = $AFsSorted[0];
            }else{
                $popMax = ".";
            }
            $varProps{ "popMax" } = $popMax;
        }
        my $format = $array[8];
        my @formatAr = split(":", $format);
        #Iterate over samples in line
        for (my $j=9; $j <= $#headerLineArray; $j++){
            my $sampleName = $headerLineArray[$j];
            my $sampleVals = $array[$j];
            if ($type eq "singlesample" && defined $sample && $sample ne ''){ #If sample switch is used
                $sampleName = $sample; #Update sampleName to be used
                if ($sampleVals =~ m|^\./\.$|gs){ #if sample value matches ./. go to next element in loop
                    next;
                }
                
            }
            my @sampleValsAr = split(":", $sampleVals);
            $varProps{ $sampleName } = $sampleVals;
            for (my $p=0; $p<=$#formatAr; $p++){
                my $prop = "$sampleName." . $formatAr[$p];
                my $val = $sampleValsAr[$p];
                $varProps{ $prop } = $val;
            }
            my @AD;
            if (exists $varProps{ "$sampleName.AD" }){ #In exceptional cases both genotypes in the file are ./. and the AD values for one of them are missing
                @AD = split(",",$varProps{ "$sampleName.AD" });
            }else{
                @AD = ("0", "0");
            }
            #Retrieve genotype to calculate VAF (needed when there are multiple alternative alles)
            my $gt = $varProps{ "$sampleName.GT" };
            #GT contains / or | symbol for alleles
            my $DP;
            my $refC = "NA";
            my $altC = "NA";
            if ($gt =~ m/([0-9]{1,1}).([0-9]{1,1})/gs){
                my $firstGT = $1;
                my $secondGT = $2;
                my $fGTidx = ($firstGT); #Genotype number is equal to index number
                my $sGTidx = ($secondGT);
                if ($firstGT == 0 && $secondGT == 0){ #If genotype is 0/0 use "1" for the second allelic depth
                    $sGTidx = 1;
                }
                $refC = $AD[$fGTidx];
                $altC = $AD[$sGTidx];
                $DP = ($refC+$altC);
            }
            if (defined $DP && $DP ne ''){ #DP sometimes isn't specified, mostly when phased genotypes occur
                #
            }else{
                $DP = 0;
            }
            my $vaf;
            if ($DP == 0){ #Sometimes DP is 0, then make $vaf == 0
                $vaf = 0;
            }else {
                $vaf = ($altC / $DP);
            }
            $varProps{ "$sampleName.REFCOUNT" } = $refC;
            $varProps{ "$sampleName.ALTCOUNT" } = $altC;
            $varProps{ "$sampleName.VAF" } = $vaf;
        }
        
        #Write output line
        my @outputLine;
        foreach my $prop (@outputHeader){
            my $val = $varProps{ $prop };
            #Some info properties don't exists for specific variants, assign these with a dot
            if (defined $val && $val ne ''){
                #bla
            }else{
                $val=".";
            }
            #print "$prop\t$val\n";
            push(@outputLine, $val);
        }
        my $outputLine = join("\t", @outputLine);
        print OUTPUT "$outputLine\n";
        undef(%varProps);
    }
    
}

close(INPUT);
close(OUTPUT);

#Retrieve and print end time
my $endtime = localtime();
print "\nFinished conversion: $endtime\n";

#Usage of software
sub usage {
        print <<EOF;
#########################################################################################################

                                convertVEPannotatedVCFtoTable.pl (version: $version)

#########################################################################################################
This script converts a VEP annotated VCF file to tabular text format
Created by: F. van Dijk
#########################################################################################################

Usage: perl convertVEPannotatedVCFtoTable.pl <PARAMETERS>
PARAMETERS:
-h\t\tThis manual
-vcf\t\tInput VCF file to convert
-tab\t\tOutput tabular *.txt file
-type\t\tType of VCF, can be either singlesample or multisample. <DEFAULT: multisample>
\t\tWhen singlesample the software assumes all columns belong to one and the same sample and merges all
\t\tsample information columns into a single column in the output tabular file
-sample\t\tSample name to use in the output tabular file. When type is singlesample this argument is
\t\tmandatory
-popmax\t\tColumns to use when extracting the population maximum allele frequency. Comma seperated list.
\t\tExample: gnomADg_AF_afr,gnomADg_AF_amr,gnomADg_AF_asj

#########################################################################################################
EOF
 
}
