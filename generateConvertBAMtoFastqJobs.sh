

PROJECTDIR="/hpc/pmc_kuiper/fvandijk2/projects/<PROJECTNAME>/"
OUTPUTDIR="/hpc/pmc_kuiper/fvandijk2/projects/<PROJECTNAME>/RESULTS/convertBamToFastq/"
JOBDIR="$PROJECTDIR/CODE/jobs/convertBamToFastq/"

#Change BAM file extension if needed, do the same 4 lines lower!!
for FILE in $(ls /hpc/pmc_kuiper/fvandijk2/projects/<PROJECTNAME>/DATA/BAM/*dedup.reformatted.bam)
do
    
SAMPLE=$( basename $FILE _dedup.reformatted.bam )
    
echo "
#$ -S /bin/bash
#$ -N convertBamtoFastq.$SAMPLE.sh
#$ -l h_vmem=10G
#$ -l tmpspace=200G
#$ -l h_rt=23:29:00
#$ -o $JOBDIR/convertBamtoFastq.$SAMPLE.sh.out
#$ -e $JOBDIR/convertBamtoFastq.$SAMPLE.sh.err
#$ -cwd

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"


set -e

#Set hostname
HOST=\$(hostname)
echo \"Executing on node: \$HOST\"

#Load module
module load samtools
module load bedtools

#Sort BAM file by read name
samtools \\
sort \\
-n $FILE \\
-o $OUTPUTDIR/$SAMPLE.qsort.bam

#Convert read name sorted BAM to FASTQ
bedtools \\
bamtofastq \\
-i $OUTPUTDIR/$SAMPLE.qsort.bam \\
-fq $OUTPUTDIR/$SAMPLE\_1.fq \\
-fq2 $OUTPUTDIR/$SAMPLE\_2.fq

#Gzip FASTQ file
cd $OUTPUTDIR
gzip $SAMPLE\_1.fq
gzip $SAMPLE\_2.fq

#Create md5sums
md5sum $SAMPLE\_1.fq.gz > $SAMPLE\_1.fq.gz.md5
md5sum $SAMPLE\_2.fq.gz > $SAMPLE\_2.fq.gz.md5
cd -


#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then
	
	echo -e \"Return code is zero, process was succesfull\n\n\"
	
else
  
	echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
	#Return non zero return code
	exit 1
	
fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"


#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"

">$JOBDIR/convertBamtoFastq.$SAMPLE.sh

done