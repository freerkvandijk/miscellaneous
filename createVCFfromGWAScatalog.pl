#!/usr/bin/perl -w
use strict;
use warnings;
use diagnostics;
use List::Util qw(min max);
use List::Util qw(sum);
use List::Util qw(first);
use List::MoreUtils qw/ uniq /;
use File::Glob ':glob';
use File::Basename;
use Getopt::Long;
use POSIX qw(floor);


##Process *.dict file to retrieve correct chromosome order
my @sortedChrs;
open(DICT, "< /hpc/pmc_kuiper/References/hg38bundle/v0/Homo_sapiens_assembly38.dict") || die "Can't open DICT file\n";
while (my $lin = <DICT>){
    chomp($lin);
    if ($lin =~ m/\@SQ\tSN:(.+)\tLN:.+/gs) { #If not header line process further
        my @array = split("\t", $lin);
        my $chr = $1;
        push(@sortedChrs, $chr);
    }
}
close(DICT);


##Read GWAS catalogue file
open(GWAS, "< /hpc/pmc_kuiper/References/GWAScatalog/gwas_catalog_v1.0.2-associations_e98_r2020-02-08.tsv") || die "Can't open GWAS file\n";
my @gwas = <GWAS>;
close(GWAS);
my $header = $gwas[0];
chomp($header);
$header =~ s/(?>\x0D\x0A?|[\x0A-\x0C\x85\x{2028}\x{2029}])//; #Remove Unix and Dos style line endings
my @headerArray = split("\t", $header);
my %chrStartLine;
my %test;
for (my $i=1; $i <= $#gwas; $i++){ #Loop over gwas catalogue file lines
    my $line = $gwas[$i];
    chomp($line);
    $line =~ s/(?>\x0D\x0A?|[\x0A-\x0C\x85\x{2028}\x{2029}])//; #Remove Unix and Dos style line endings
    my @array = split("\t", $line);
    my $chr = $array[11];
    my $pos = $array[12];
    if (defined $chr && $chr ne '' && defined $pos && $pos ne ''){
        #check if chr contains ; or x character
        if (index($chr, ";") != -1) { #Example: 2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2
            my $chrSplitcount = $chr =~ tr/;//; #Count how many times columns can be split
            #print "$chr contains ;\n";
            my @tmpArray=split(";", $chr); #Retrieve number of SNPs to process (iterations to do)
            my $lastIdx = $#tmpArray; #number of times to iterate over this specific line
            my @lineToProc = split("\t", $line); #Split line, so elements can be processed individually
            for (my $j=0; $j <= $lastIdx; $j++){ #Iterate X times 
                #my $outputLine;
                my @newLine;
                for (my $k=0; $k<=$#lineToProc; $k++){ #Iterate over line to process, each element is a column
                    my $elem = $lineToProc[$k];
                    if ( defined $elem && $elem ne ''){
                        $elem =~ s/; /;/gs; #Replace like this, otherwise spaces in normal text fields also get replaced
                    }else{
                        $elem = "NA"; #Put empty elements to NA
                    }
                    my $elemToPrint = $elem;
                    #Check if element in "splittable"
                    if (index($elem, ";") != -1){ #if splittable, add correct index to output array
                        my $count = $elem =~ tr/;//; #Count how many times ";" occurs in field
                        if ($chrSplitcount == $count){ #if number of ";" in chr field matches number of ";" in this field do split, otherwise don't
                            my @ar = split(";", $elem);
                            $elemToPrint = $ar[$j];
                            if ($k == 11){ #update chr number
                                $chr = $elemToPrint;
                            }
                            if ($k == 12){ #update pos number
                                $pos = $elemToPrint;
                            }
                        }else{
                            $elemToPrint = $elem;
                        }
                    }else{
                        $elemToPrint = $elem;
                    }
                    push(@newLine, $elemToPrint); #Push element in array to build output string from
                }
                
                my $outputLine = join("\t",@newLine); #Join array to string
                #print "LINE: $outputLine\n";
                $chrStartLine{ "chr$chr" }{ $pos }{ $outputLine } = $outputLine; #Push in hashXhashXhash
                @newLine=(); #empty array
            }
            
        }elsif (index($chr, " x ") != -1){ #Example: 8 x 9
            my $chrSplitcount = $chr =~ tr/ x //; #Count how many times columns can be split
            #print "$chr contains  x \n";
            my @tmpArray=split(" x ", $chr); #Retrieve number of SNPs to process (iterations to do)
            my $lastIdx = $#tmpArray; #number of times to iterate over this specific line
            my @lineToProc = split("\t", $line); #Split line, so elements can be processed individually
            for (my $j=0; $j <= $lastIdx; $j++){ #Iterate X times 
                #my $outputLine;
                my @newLine;
                for (my $k=0; $k<=$#lineToProc; $k++){ #Iterate over line to process, each element is a column
                    my $elem = $lineToProc[$k];
                    if ($elem eq ''){ #if empty fill with NA
                        $elem = "NA";
                    }
                    my $elemToPrint = $elem;
                    #Check if element in "splittable"
                    if (index($elem, " x ") != -1){ #if splittable, add correct index to output array
                        my $count = $elem =~ tr/ x //; #Count how many times ";" occurs in field
                        if ($chrSplitcount == $count){ #if number of " x " in chr field matches number of " x " in this field do split, otherwise don't
                            my @ar = split(" x ", $elem);
                            if ($k == 11){ #update chr number
                                $elemToPrint = $ar[$j];
                                $chr = $elemToPrint
                            }
                            if ($k == 12){ #update pos number
                                $elemToPrint = $ar[$j];
                                $pos = $elemToPrint;
                            }
                        }else{
                            $elemToPrint = $elem;
                        }
                    }else{
                        $elemToPrint = $elem;
                    }
                    push(@newLine, $elemToPrint); #Push element in array to build output string from
                }
                
                my $outputLine = join("\t",@newLine); #Join array to string
                #print "LINE: $outputLine\n";
                $chrStartLine{ "chr$chr" }{ $pos }{ $outputLine } = $outputLine; #Push in hashXhashXhash
                @newLine=(); #empty array
            }
            
            
        }else{
            #print "$chr is single\n";
            my @newLine;
            my @lineToProc = split("\t", $line); #Split line, so elements can be processed individually
            for (my $k=0; $k<=$#lineToProc; $k++){ #Iterate over line to process, each element is a column
                my $elem = $lineToProc[$k];
                if ($elem eq ''){ #if empty fill with NA
                    $elem = "NA";
                }
                my $elemToPrint = $elem;
                push(@newLine, $elemToPrint);
            }
            
            my $outputLine = join("\t",@newLine); #Join array to string
            #print "LINE: $outputLine\n";
            $chrStartLine{ "chr$chr" }{ $pos }{ $outputLine } = $outputLine; #Push in hashXhashXhash
            @newLine=(); #empty array
        }

    }
}


##Create output file
print "\nCreating output VCF file..\n";
open(OUTPUT, "> /hpc/pmc_kuiper/References/GWAScatalog/gwas_catalog_v1.0.2-associations_e98_r2020-02-08.vcf") || die "Can't open MET file\n";

#Print header
print OUTPUT <<EOF;
##fileformat=VCFv4.1
##source=gwas_catalog_v1.0.2
##reference=GRCh38
##fileDate=2020-02-08
#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO
EOF

#print body
foreach my $chrom (@sortedChrs){ #Loop over chroms from dict file
    if (exists $chrStartLine{ $chrom }){
        #Sort start positions
        foreach my $pos (sort {$a <=> $b} keys %{ $chrStartLine{ $chrom } }){ #sort positions
            foreach my $line (sort keys %{ $chrStartLine{ $chrom }{ $pos } }){ #retrieve lines
            #my $line = $chrStartLine{ $chrom }{ $pos };
            my @array = split("\t", $line);
            my @infoArray;
            for (my $j=0 ; $j <= $#headerArray; $j++){ #Create key value pairs for VCF file INFO line
                my $key = $headerArray[$j];
                my $value = $array[$j];
                $key =~ s/ /_/gs; #Replace spaces with underscore
                $value =~ s/ /_/gs;
                if (defined $value && $value eq ''){ #If empty value fill with NA
                    $value = "NA";
                }
                my $pair = "$key=$value";
                push(@infoArray, $pair); #push key/value in array
            }
            my $infoString = join(";", @infoArray); #Join arry into string
            print OUTPUT "$chrom\t$pos\t.\tN\tN\t.\t.\t$infoString\n"; #Print output VCF line
            }
        }
    }else{
        #print "Non-existing chromosome: $chrom\n";
    }
}
close(OUTPUT);
print "Done creating output VCF file.\n\n";





